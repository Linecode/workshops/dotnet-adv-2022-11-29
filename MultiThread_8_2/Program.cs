﻿using System;
using System.Threading;

namespace MultiThread_8_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            var t = new Thread(() =>
            {
                while (true)
                {
                    DumpThreadInfo();
                }
            }) { IsBackground = true };
            
            t.Start();

            Console.ReadLine();
        }
        
        static void DumpThreadInfo()
        {
            Console.WriteLine($"Thread: id: {Thread.CurrentThread.ManagedThreadId} " +
                              $"isThreadPoolThread: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"isBackground: {Thread.CurrentThread.IsBackground}");
        }
    }
}