using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq_Playground.Domain
{
    public enum MovieState
    {
        New,
        InProgress,
        Complete
    }
    
    public class Movie
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public MovieParty Director { get; set; }
        public MovieState State { get; set; } = MovieState.New;
        public DateTime AvailableFrom { get; set; } = DateTime.UtcNow.Subtract(TimeSpan.FromDays(-1));
        public DateTime AvailableTo { get; set; } = DateTime.UtcNow.Subtract(TimeSpan.FromDays(-1));
        public List<MovieParty> Cast { get; set; } = new ();

        public override string ToString()
        {
            return $"MovieId: {Id} \nTitle: {Title} \nDirector: {Director.FirstName} {Director.LastName} \n" +
                   $"Cast: {Cast?.Select(x => $"  {x.Role}: {x.FirstName} {x.LastName}").Aggregate(string.Empty, (c, n) => $"{c} \n{n}")}";
        }
    }
}