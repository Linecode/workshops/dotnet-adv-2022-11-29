using System;
using System.Linq.Expressions;

namespace Linq_Playground.Domain.Specifications;

public class InProgressMovieSpecification : Specification<Movie>
{
    public override Expression<Func<Movie, bool>> ToExpression()
    {
        return movie => movie.State == MovieState.InProgress;
    }
}

public class AvailableMovieSpecification : Specification<Movie>
{
    public override Expression<Func<Movie, bool>> ToExpression()
    {
        return movie => movie.AvailableFrom <= DateTime.UtcNow && movie.AvailableTo >= DateTime.UtcNow;
    }
}