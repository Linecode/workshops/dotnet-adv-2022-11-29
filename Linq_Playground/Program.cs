﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Linq_Playground.Domain;
using Linq_Playground.Domain.Specifications;
using Linq_Playground.Infrastructure;

namespace Linq_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // var sentence = "the quick brown fox jumped over the lazy dog";
            // var words = sentence.Split(' ');
            //
            // var query = from word in words
            //     group word.ToUpper() by word.Length
            //     into gr
            //     orderby gr.Key
            //     select new { Length = gr.Key, Words = gr };

            // var query2 = words.GroupBy(w => w.Length, w => w.ToUpper())
            //     .Select(x => new { Length = x.Key, Words = x })
            //     .OrderBy(x => x.Length);
            //
            // words.Map(x => new { x.Length, Upper = x.ToUpper() })
            //     .MyOrderBy()
            //     .Count();

            // var repository = new Repository();
            //
            // var allowList = Enumerable.Range(0, 1)
            //     .Select(x => Guid.NewGuid()).ToList();

            // var moviesTitles = repository.GetAll()
            //     .SelectMany(x => x.Cast)
            //     .Where(x => allowList.Contains(x.Id))
            //     .Take(10) // Pobiera 10 elementów
            //     .Skip(1); // Pomija pierwszy element
            //     // .Select(x => x.Title.Length)
            //     // .Sum();
            //     // .Where(x => x.Contains("e"));
            //     // .FirstOrDefault(x => x.Contains("e"));
            //     // .FirstOrDefault(x => x < 0);
            //     // .SelectMany(x => x.Cast)
            //     // .Distinct()
            //     // .Select(x => new { x.FirstName, x.LastName });
            //     // .Single() // => zwraca pojedyńczy element jeżeli tablica zawiera pojedyńczy element
            //     // .SignleOrDefault () // zwraca pojedyńczy element jeżeli tablica zawiera pojedyńczy element lub jest pusta (wtedy otrzymujemy defaultową wartoś)
            //     // .First()
            //     // .Last();
            //     // .Aggregate(string.Empty, (a, b) => $"{a}|{b.FirstName} {b.LastName}");
                
            // moviesTitles.ForEach(x =>
            // {
            //     Console.WriteLine($"{x.FirstName} {x.LastName}");
            //     var index = moviesTitles.IndexOf(x);
            //     moviesTitles.ElementAt(index).FirstName = $"{x.FirstName} {x.LastName}";
            // });

            // Console.WriteLine(moviesTitles);
            //     
            // Console.ReadLine();
            //
            // foreach (var title in moviesTitles)
            // {
            //     Console.WriteLine(moviesTitles);
            // }

            BinaryExpression be = Expression.Power(Expression.Constant(2D), Expression.Constant(3D));

            Expression<Func<double>> le = Expression.Lambda<Func<double>>(be);

            Func<double> compiledExpression = le.Compile();

            var result = compiledExpression();
            
            Console.WriteLine(result);

            ParameterExpression numParm = Expression.Parameter(typeof(int), "num");
            ConstantExpression five = Expression.Constant(5, typeof(int));
            BinaryExpression numLessThanFive = Expression.LessThan(numParm, five);
            Expression<Func<int, bool>> lambda = Expression.Lambda<Func<int, bool>>(numLessThanFive,
                new []
                {
                    numParm
                });

            Func<int, bool> compiled = lambda.Compile();

            var result2 = compiled(3);
            
            var specification = new InProgressMovieSpecification()
            .And(new AvailableMovieSpecification());

            var spec2 = new InProgressMovieSpecification() & new AvailableMovieSpecification();

            var movie = new Movie();

            specification.IsSatisfiedBy(movie);
        }
    }

    // public static class MyLinq
    // {
    //     public static IEnumerable<TResult> Map<T, TResult>(this IEnumerable<T> input, Func<T, TResult> action)
    //     {
    //         var newList = new List<TResult>();
    //
    //         foreach (var item in input)
    //         {
    //             newList.Add(action(item));
    //         }
    //
    //         return newList;
    //     }
    //
    //     public static IEnumerable<T> MyOrderBy<T>(this IEnumerable<T> input)
    //     {
    //         return input.Reverse();
    //     }
    //
    //     public static int Count(this IEnumerable input)
    //     {
    //         return input.Count();
    //     }
    // }

    public abstract class Specification<T>
    {
        public abstract Expression<Func<T, bool>> ToExpression();

        public bool IsSatisfiedBy(T entity)
        {
            var predicate = ToExpression().Compile();
            return predicate(entity);
        }

        public Specification<T> And(Specification<T> specification)
        {
            return new AndSpecification<T>(this, specification);
        }

        public Specification<T> Or(Specification<T> specification)
        {
            return new OrSpecification<T>(this, specification);
        }
        
        public static Specification<T> operator &(Specification<T> left, Specification<T> right)
            => new AndSpecification<T>(left, right);
    }

    public class AndSpecification<T> : Specification<T>
    {
        private readonly Specification<T> _left;
        private readonly Specification<T> _right;

        public AndSpecification(Specification<T> left, Specification<T> right)
        {
            _left = left;
            _right = right;
        }

        public override Expression<Func<T, bool>> ToExpression()
        {
            Expression<Func<T, bool>> leftExpression = _left.ToExpression();
            Expression<Func<T, bool>> rightExpression = _right.ToExpression();

            var paramExpr = Expression.Parameter(typeof(T));
            var exprBody = Expression.AndAlso(leftExpression.Body, rightExpression.Body);
            exprBody = (BinaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);
            var finalExpr = Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);

            return finalExpr;
        }
    }

    public class OrSpecification<T> : Specification<T>
    {
        private readonly Specification<T> _left;
        private readonly Specification<T> _right;

        public OrSpecification(Specification<T> left, Specification<T> right)
        {
            _left = left;
            _right = right;
        }

        public override Expression<Func<T, bool>> ToExpression()
        {
            Expression<Func<T, bool>> leftExpression = _left.ToExpression();
            Expression<Func<T, bool>> rightExpression = _right.ToExpression();

            var paramExpr = Expression.Parameter(typeof(T));
            var exprBody = Expression.OrElse(leftExpression.Body, rightExpression.Body);
            exprBody = (BinaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);
            var finalExpr = Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);

            return finalExpr;
        }
    }

    public class ParameterReplacer : ExpressionVisitor
    {
        private readonly ParameterExpression _parameter;

        protected override Expression VisitParameter(ParameterExpression node)
            => base.VisitParameter(_parameter);

        internal ParameterReplacer(ParameterExpression parameter)
        {
            _parameter = parameter;
        }
    }
}