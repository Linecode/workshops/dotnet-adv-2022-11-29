using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using Linq_Playground.Domain;

namespace Linq_Playground.Infrastructure
{
    public class Repository
    {
        private List<Movie> _movies;

        public Repository()
        {
            #region dataGenerator
            var nonDirectorRoles = new[] { MoviePartyRole.Actor, MoviePartyRole.Stuff };

            var directorGenerator = new Faker<MovieParty>()
                .RuleFor(x => x.FirstName, f => f.Person.FirstName)
                .RuleFor(x => x.LastName, f => f.Person.LastName)
                .RuleFor(x => x.Role, _ => MoviePartyRole.Director)
                .RuleFor(x => x.Id, _ => Guid.NewGuid());

            var actorOrStuffGenerator = new Faker<MovieParty>()
                .RuleFor(x => x.FirstName, f => f.Person.FirstName)
                .RuleFor(x => x.LastName, f => f.Person.LastName)
                .RuleFor(x => x.Id, f => Guid.NewGuid())
                .RuleFor(x => x.Role, f => f.PickRandom(nonDirectorRoles));

            var movieGenerator = new Faker<Movie>()
                .RuleFor(x => x.Id, _ => Guid.NewGuid())
                .RuleFor(x => x.Title, f => f.Lorem.Sentence())
                .RuleFor(x => x.Director, _ => directorGenerator.Generate())
                .RuleFor(x => x.Cast, f => actorOrStuffGenerator.Generate(f.Random.Int(8, 20)));

            _movies = movieGenerator.Generate(new Randomizer().Int(5, 15));
            #endregion
        }

        public List<Movie> GetAll()
            => _movies;

        public void Add(Movie movie)
            => _movies.Add(movie);

        public Movie Get(Guid movieId)
            => _movies.FirstOrDefault(x => x.Id == movieId);

        public void Remove(Guid movieId)
            => _movies.Remove(Get(movieId));

        public void Remove(Movie movie)
            => _movies.Remove(movie);
    }
}