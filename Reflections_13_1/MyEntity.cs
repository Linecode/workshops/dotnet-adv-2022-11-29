using System;
using System.ComponentModel;
using Reflections_13_1_Lib;

namespace Reflections_13_1
{
    [My("Smth")]
    public class MyEntity
    {
        public string Secret { get; private set; }
        
        public string MoreSecret { get; }
        
        private MyEntity() {}

        private MyEntity(string _)
        {
            MoreSecret = _;
        }
        public MyEntity(string _, string __, string ___) {}
        
        public void Test1() {}
        public void Test2() {}
        public void Test3() {}
        private void Test4() {}
        private void Test5() {}
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MyAttribute : Attribute
    {
        public string Value { get; }
        
        public MyAttribute(string value)
        {
            Value = value;
        }
    }

    public class A : IMarker {}
    public class B : IMarker {}
    public class C : IMarker {}
}