﻿using System;
using System.Threading;

namespace Network_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var t = new Thread(() =>
            {
                var sever = new Server("127.0.0.1", 5678);
            });
            t.Start();
            
            Console.ReadLine();
        }
    }
}