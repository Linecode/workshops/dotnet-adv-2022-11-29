﻿using System;
using System.Reflection;
using Autofac;

namespace IoC_Autofac_Simple
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
 
// Register individual components
            builder.RegisterInstance(new TaskRepository())
                .As<ITaskRepository>();
            builder.RegisterType<TaskController>();
            builder.Register(c => new LogManager(DateTime.Now))
                .As<ILogger>();
 
// Scan an assembly for components
            builder.RegisterAssemblyTypes(typeof(Program).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces();
 
            var container = builder.Build();

            using (var scope = container.BeginLifetimeScope())
            {
                var taskRepository = scope.Resolve<ITaskRepository>();

                Console.WriteLine("");
            }
        }
    }

    internal interface ILogger
    {
    }

    internal class LogManager : ILogger
    {
        public LogManager(DateTime now)
        {
            
        }
    }

    internal class TaskController
    {
    }

    internal interface ITaskRepository
    {
    }

    internal class TaskRepository : ITaskRepository
    {
    }

    internal class SomeRepository
    {
        
    }
}