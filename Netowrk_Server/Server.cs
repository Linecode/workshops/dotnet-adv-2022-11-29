using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Netowrk_Server;

public class Server
{
    private TcpListener server = null;

    public Server(string ip, int port)
    {
        var address = IPAddress.Parse(ip);

        server = new TcpListener(address, port);
        server.Start();
            
        StartListener();
    }

    private void StartListener()
    {
        try
        {
            while (true)
            {
                Console.WriteLine("Waiting for a connection...");

                var client = server.AcceptTcpClient();

                Console.WriteLine("Connected!");

                var t = new Thread(Handle);
                t.Start(client);
            }
        }
        catch (SocketException e)
        {
            Console.WriteLine($"Socket Exception {e.Message}");
            server.Stop();
        }
    }

    private void Handle(object? obj)
    {
        var client = (TcpClient)obj;

        try
        {
            string message = "";

            while (message != null && !message.StartsWith("quit"))
            {
                byte[] data = Encoding.ASCII.GetBytes("Send next data: [enter 'quit' to terminate]");
                client?.GetStream().Write(data, 0, data.Length);

                byte[] buffer = new byte[1024];
                client?.GetStream().Read(buffer, 0, buffer.Length);

                message = Encoding.ASCII.GetString(buffer);
                Console.WriteLine(message);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception {0}", ex.Message);
            client?.Close();
        }
    }
}