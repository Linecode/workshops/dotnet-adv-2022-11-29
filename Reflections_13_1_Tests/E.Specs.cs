using System;
using System.Linq;
using FluentAssertions;
using Reflections_13_1;
using Xunit;

namespace Reflections_13_1_Tests
{
    public abstract class E_Specs
    {
        public class E1 : E_Specs
        {
            [Fact]
            public void RunTest()
            {
                var entity = (MyEntity)Activator.CreateInstance(typeof(MyEntity),true);
                var result = Exercises.E1(entity);

                result.Should().NotBeNull();
                result.Secret.Should().Be("NotSoMuch");
            }
        }

        public class E2 : E_Specs
        {
            [Fact]
            public void RunTest()
            {
                var methods = new string[] { "Test1", "Test2", "Test3", "Test4", "Test5" };
                var result = Exercises.E2();

                result.Should().NotBeNull();

                result.Should().Contain(x => methods.Contains(x.Name));
            }
        }

        public class E3 : E_Specs
        {
            [Fact]
            public void RunTest()
            {
                var result = Exercises.E3();

                result.Should().Be("Smth");
            }
        }

        public class E4 : E_Specs
        {
            [Fact]
            public void RunTest()
            {
                var result = Exercises.E4();

                result.Should().NotBeNull();
                result.Should().BeAssignableTo<MyEntity>();
            }
        }

        public class E5 : E_Specs
        {
            [Fact]
            public void RunTest()
            {
                var types = new string[] { "A", "B", "C", "D" };
                var result = Exercises.E5();

                result.Should().Contain(x => types.Contains(x.Name))
                    .And.NotContain(x => x.Name.Equals("IMarker"));
            }
        }

        public class E6 : E_Specs
        {
            [Fact]
            public void RunTest()
            {
                var result = Exercises.E6();

                result.Should().NotBeNull();
                result.MoreSecret.Should().Be("Test Test");
            }
        }
    }
}