﻿using System.Collections.Concurrent;

var restaurant = new Restaurant();
restaurant.Start();

Console.ReadLine();

public class Restaurant
{
    private Waiter[] _waiters = new[]
    {
        new Waiter("John" , 500, 500),
        new Waiter("Mark", 2000, 2000),
    };

    private Chief[] _chiefs = new[]
    {
        new Chief("Nick", 5000),
        new Chief("Monica", 1000),
    };
    
    private readonly ConcurrentQueue<Dish> _orders = new();
    private readonly ConcurrentQueue<Dish> _ready = new();

    public void Start()
    {
        _waiters.ToList().Select(x =>
        {
            Waiter temp = x;
            return Task.Run(async () =>
            {
                while (true)
                {
                    await temp.TakeOrder(_orders);
                    await temp.ServeOrder(_ready);
                }
            });
        }).ToList();

        _chiefs.ToList().Select(x =>
        {
            Chief temp = x;
            return Task.Run(async () =>
            {
                while (true)
                {
                    await temp.PrepareDish(_orders, _ready);
                }
            });
        }).ToList();
    }
}

public record Dish(string Name);

public record Chief(string Name, int PrepareDelay)
{
    public async Task PrepareDish(ConcurrentQueue<Dish> ordersQueue, ConcurrentQueue<Dish> readyQueue)
    {
        Console.WriteLine($"Chief {Name}: is starting prepering dish");

        var haveDish = ordersQueue.TryDequeue(out var dish);

        if (haveDish)
        {
            await Task.Delay(PrepareDelay);
            Console.WriteLine($"Chief {Name}: complete dish");
            readyQueue.Enqueue(dish!);
        }
        else
        {
            Console.WriteLine($"Chief {Name}: have no dish to prepare");
            await Task.Delay(1000);
        }
    }
};

public record Waiter(string Name, int TakeDelay, int ServeDelay)
{
    public async Task TakeOrder(ConcurrentQueue<Dish> queue)
    {
        Console.WriteLine($"Waiter {Name}: is taking order");
        await Task.Delay(TakeDelay);
        queue.Enqueue(new Dish(Guid.NewGuid().ToString()));
    }

    public async Task ServeOrder(ConcurrentQueue<Dish> queue)
    {
        var haveDish = queue.TryDequeue(out var dish);

        if (haveDish)
        {
            Console.WriteLine($"Waiter {Name}: is serving order");
            await Task.Delay(ServeDelay);
        }
        else
        {
            Console.WriteLine($"Waiter {Name}: have no dishes to serve");
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
    }
}; 