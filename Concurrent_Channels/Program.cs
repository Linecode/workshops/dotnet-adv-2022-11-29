﻿// See https://aka.ms/new-console-template for more information

using System.Threading.Channels;

var channel = Channel.CreateUnbounded<string>();

var producer1 = new Producer(channel.Writer, 1, 2000);
var consumer1 = new Consumer(channel.Reader, 1, 1500);

Task consumerTask1 = consumer1.ConsumeData();
Task producerTask1 = producer1.BeginProducing();

await producerTask1.ContinueWith(_ => channel.Writer.Complete());
await consumerTask1;

class Producer
{
    private readonly ChannelWriter<string> _writer;
    private readonly int _identifier;
    private readonly int _delay;

    public Producer(ChannelWriter<string> writer, int identifier, int delay)
    {
        _writer = writer;
        _identifier = identifier;
        _delay = delay;
    }

    public async Task BeginProducing()
    {
        Console.WriteLine($"[Producer: {_identifier}]: starting");

        for (int i = 0; i < 10; i++)
        {
            await Task.Delay(_delay);

            var msg = $"P{_identifier} - {DateTime.UtcNow:G}";
            
            Console.WriteLine($"[Producer: {_identifier}]: Creating message: {msg}");

            await _writer.WriteAsync(msg);
        }
        
        Console.WriteLine($"[Producer: {_identifier}]: Finished");
    }
}

class Consumer
{
    private readonly ChannelReader<string> _reader;
    private readonly int _identifier;
    private readonly int _delay;

    public Consumer(ChannelReader<string> reader, int identifier, int delay)
    {
        _reader = reader;
        _identifier = identifier;
        _delay = delay;
    }

    public async Task ConsumeData()
    {
        Console.WriteLine($"[Consumer: {_identifier}]: starting");

        while (await _reader.WaitToReadAsync())
        {
            if (_reader.TryRead(out var timeString))
            {
                await Task.Delay(_delay);

                Console.WriteLine($"[Consumer: {_identifier}]: Consumed msg: {timeString}");
            }
        }
        
        Console.WriteLine($"[Consumer: {_identifier}]: Finished");
    }
}