﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Reflections_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj = new object();

            var a = typeof(Interlocked);
            
            var type = "".GetType();

            var v = (Vault)Activator.CreateInstance(typeof(Vault), true);

            var t = typeof(Vault);
            var paramTypes = new Type[] { typeof(string), typeof(string) };
            var paramValues = new object[] { "hahaha1", "hahaha2" };
            var constructor = t.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, paramTypes, null);

            var v2 = (Vault)constructor?.Invoke(paramValues);
            
            typeof(Vault).GetProperty(nameof(Vault.Value2))!
                .SetValue(v2, "not so secret");
            
            typeof(Vault).GetProperty("Value", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(v2, "hacked!!");
            
            // ----------------------------------------------------------------
            var attr = System.Attribute.GetCustomAttributes(typeof(Vault))
                .Where(x => x is MyAttribute)
                .Cast<MyAttribute>()
                .First();

            if (attr.Name.Equals("Secret"))
            {
                // do smth
            } 
            
            // ----------------------------------------------------------------
            var itype = typeof(IMarker);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => x.IsClass)
                .Where(x => itype.IsAssignableFrom(x))
                .ToList();
            
            // ------------------------------

            //typeof(Generic<int>);
            var genericType = typeof(Generic<>).MakeGenericType(typeof(int));
            var genericObject = (Generic<int>)Activator.CreateInstance(genericType);
            
            
            // ------------------------------

            var methods = typeof(Vault).GetMethods(BindingFlags.Instance | BindingFlags.Public);
            
            Console.ReadLine();
        }
    }

    [My("ndjsakndkjsadnkjsa")]
    public class Vault
    {
        public string Value2 { get; init; }
        private string Value { get; set; }

        private Vault()
        {
            Console.WriteLine("Hello from my private constructor");
        }

        private Vault(string _, string __)
        {
            Console.WriteLine($"{_}-{__}");
        }

        public void SomeAction()
        {
            
        }

        public void SomeActionV2()
        {
            
        }
    }

    public class Model
    {
        private List<int> _list = new List<int>();

        public IReadOnlyList<int> List => _list;
        
        public string Test { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MyAttribute : System.Attribute
    {
        public string Name { get; }

        public MyAttribute(string name)
        {
            Name = name;
        }
    }

    [My("dsandnsaksajk")]
    public class MyClass {}

    public interface IMarker
    { }

    public class A : IMarker
    { }
    
    public class B : IMarker {}

    public class Generic<T>
    {
        public T Value { get; set; }
    }
}