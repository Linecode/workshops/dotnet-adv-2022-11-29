﻿// See https://aka.ms/new-console-template for more information

using System.Collections.Concurrent;

var threadSafeCache = new ThreadSafeCache();

Action<int> saveAction = (id) =>
{
    threadSafeCache.Save(id.ToString(), new EntityToTest(Guid.NewGuid().ToString()));
};

Action<int> getAction = async (id) =>
{
    await Task.Delay(100);
    var entity = threadSafeCache.Load<EntityToTest>(id.ToString());
    Console.WriteLine($"Read entity with id: {entity.Smth}");
};

Task[] tasks = new Task[4];
for (int i = 0; i < tasks.Length; i++)
    tasks[i] = Task.Factory.StartNew((x) => saveAction((int)x!), i);

Task[] post = new Task[4];
for (int i = 0; i < post.Length; i++)
    post[i] = Task.Factory.StartNew((x) => getAction((int)x!), i);

Console.ReadLine();

public class ThreadSafeCache
{
    private readonly ConcurrentDictionary<string, object?> _cache = new();

    public void Save<T>(string key, T value)
    {
        _cache.AddOrUpdate(key, value, (s, o) => value);
    }

    public T Load<T>(string key)
    {
        var exists = _cache.TryGetValue(key, out object? value);
        
        if (exists)
        {
            return (T)value!;
        }

        return default!;
    }
}

public record EntityToTest(String Smth);