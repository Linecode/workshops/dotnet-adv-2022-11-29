``` ini

BenchmarkDotNet=v0.13.1, OS=macOS Big Sur 11.5.2 (20G95) [Darwin 20.6.0]
Intel Core i7-1068NG7 CPU 2.30GHz, 1 CPU, 8 logical and 4 physical cores
.NET SDK=5.0.100
  [Host]     : .NET 5.0.0 (5.0.20.51904), X64 RyuJIT
  DefaultJob : .NET 5.0.0 (5.0.20.51904), X64 RyuJIT


```
|                   Method |     Mean |     Error |    StdDev |
|------------------------- |---------:|----------:|----------:|
|            GetPrimePLinq | 1.881 ms | 0.0374 ms | 0.0712 ms |
|                 GetPrime | 5.424 ms | 0.0238 ms | 0.0199 ms |
| GetPrimeListWithParallel | 1.761 ms | 0.0280 ms | 0.0262 ms |
