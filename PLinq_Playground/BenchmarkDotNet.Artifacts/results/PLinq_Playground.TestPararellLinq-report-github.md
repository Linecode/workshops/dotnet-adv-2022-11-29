``` ini

BenchmarkDotNet=v0.13.1, OS=macOS Monterey 12.4 (21F79) [Darwin 21.5.0]
Apple M1 Max, 1 CPU, 10 logical and 10 physical cores
.NET SDK=6.0.301
  [Host]     : .NET 6.0.6 (6.0.622.26707), Arm64 RyuJIT
  DefaultJob : .NET 6.0.6 (6.0.622.26707), Arm64 RyuJIT


```
|               Method |       Mean |    Error |    StdDev |
|--------------------- |-----------:|---------:|----------:|
|         GetPrimeLinq | 2,523.7 μs |  2.80 μs |   2.48 μs |
| GetPrimeParrarelLinq |   795.9 μs | 15.27 μs |  15.68 μs |
|     GetPrimeParrarel | 2,551.4 μs | 50.57 μs | 142.63 μs |
