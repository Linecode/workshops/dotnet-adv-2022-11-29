﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using BenchmarkDotNet.Toolchains.InProcess.NoEmit;

namespace PLinq_Playground
{
    using System;
    using System.Security.Cryptography;
    using BenchmarkDotNet.Attributes;
    using BenchmarkDotNet.Running;
    
    public class Program
    {
        public static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run(typeof(Program).Assembly);
        }
    }

    public class TestPararellLinq
    {
        private IEnumerable<int> source = Enumerable.Range(0, 100_000).ToList();

        [Benchmark]
        public IList<int> GetPrimeLinq() => source.Where(IsPrime).ToList();

        [Benchmark]
        public IList<int> GetPrimeParrarelLinq() => source.AsParallel().Where(IsPrime).ToList();

        [Benchmark]
        public IList<int> GetPrimeParrarel()
        {
            var concurrentQueue = new ConcurrentQueue<int>();
            Action<int, int> action = (skip, take) =>
            {
                var element = source.Skip(skip).Take(take);

                foreach (var i in element)
                {
                    if (IsPrime(i))
                    {
                        concurrentQueue.Enqueue(i);
                    }
                }
            };
            Parallel.Invoke(() => action(0,50000), () => action(50000, 100000));
            
            return concurrentQueue.ToList();
        }

        private static bool IsPrime(int number)
        {
            if (number < 2)
            {
                return false;
            }

            for (var divisor = 2; divisor <= Math.Sqrt(number); divisor++)
            {
                if (number % divisor == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}