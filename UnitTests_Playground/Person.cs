namespace UnitTests_Playground;

public class Person
{
    public string FirstName { get; private set; }
    public string LastName { get; private set; }
    public int Age { get; private set; }

    public Person(string firstName, string lastName, int age)
    {
        FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
        LastName = lastName ?? throw new ArgumentNullException(nameof(lastName));
        Age = age;
    }

    public bool IsOver18()
    {
        return Age >= 18;
    }

    public bool AreNamesValid()
    {
        if (string.IsNullOrEmpty(FirstName))
            return false;
        
        if (string.IsNullOrEmpty(LastName))
            return false;
        
        return true;
    }
}