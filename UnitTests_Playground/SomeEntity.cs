namespace UnitTests_Playground;

public class SomeEntity
{
    public string Name { get; set; }
    public string Name2 { get; set; }

    public string Concat => $"{Name} {Name2}";

    public int ToTest(string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            return 0;
        }
        return name.Length;
    }

    public int ToTest2(int a)
    {
        return a*a;
    }
}