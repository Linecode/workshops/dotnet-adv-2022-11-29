using Autofac;

namespace IoC_Autofac_WebApi.Module
{
    public class TestModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(TestModule).Assembly)
                .PublicOnly()
                .AsImplementedInterfaces();
        }
    }
}