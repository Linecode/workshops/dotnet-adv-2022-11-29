using IoC_Autofac_WebApi.Repositories;
using IoC_Autofac_WebApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace IoC_Autofac_WebApi.Controllers
{
    [ApiController]
    public class SomeController : Controller
    {
        private readonly IService _service;
        private readonly IRepository _repository;
        private readonly SomeType _someType;

        public SomeController(IService service, IRepository repository, SomeType someType)
        {
            _service = service;
            _repository = repository;
            _someType = someType;
        }
        
        // GET
        public IActionResult Index()
        {
            return Ok();
        }
    }
}