using System;

namespace IoC_Autofac_WebApi.Services
{
    public interface IService {}
    public class Service1 : IService
    {
        
    }
    
    public class Service2 : IService {}
    
    public class Service3 : IService {}

    public class SomeType
    {
        public void SomeMethod()
        {
            Console.WriteLine("Some Log");
        }
    }
}