﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Collections_4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            double b;
            string action;

            var actions = new Dictionary<string, Func<double, double, double>>
            {
                { "+", (x, y) => x + y },
                { "-", (x, y) => x - y },
                { "*", (x, y) => x * y },
                { "/", (x, y) => x / y },
            };
            
            Console.WriteLine("Numer 1: ");
            a = Convert.ToDouble(Console.ReadLine());
            
            Console.WriteLine("Numer 2: ");
            b = Convert.ToDouble(Console.ReadLine());
            
            Console.WriteLine("Action: ");
            action = Console.ReadLine();

            actions.TryGetValue(action!, out var func);
            
            var result = func?.Invoke(a, b);
            
            Console.WriteLine($"Wynik to: {result}");
        }
    }
}