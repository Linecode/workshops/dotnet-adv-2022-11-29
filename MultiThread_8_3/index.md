# Zadanie 8_3

Wykorzystaj api ThreadPool (klasa `ThreadPool`) aby: 
- Stworzyć dwa wątki
  - Pierwszy z nich niech odlicza co sekundę w dół
  - Natomiast durgi z nich niech odlicza co sekundę w górę

Wymagania:
- W momencie kiedy pierwszy wątek osiągnie liczbę -10 lub został wywołany cancel na cancelletion Token zakończ wątek
- W momencie kiedy drugi wątek osiągnie liczbę 15 lub został wywołany cancel na cancelletion Token  zakończ wątek
