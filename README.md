# Warsztaty dotnet: poziom zaawansowany

*Zachęcam do pracy na swoim własnym dedykowanym branchu*

## Tworzenie branch-a

### Konsola

```bash
git clone https://gitlab.com/Linecode/workshops/dotnet-adv-2022-11-29.git
cd dotnet-adv-2022-11-29
git checkout -b unikalny-ciag-znakow
```

### Rider

![rider-1](./.assets/rider-git-1.png)

![rider-2](./.assets/rider-git-2.png)

![rider-3](./.assets/rider-git-3.png)

## Visual Studio

![visual-1](./.assets/visual-git-1.png)

![visual-2](./.assets/visual-git-2.png)

![visual-3](./.assets/visual-git-3.png)

## Uruchomienie Testów Jednostkowych

### Rider

![rider-tests](./.assets/rider-tests.png)

### Visual Studio

![visual-tests](./.assets/visual-tests.png)
