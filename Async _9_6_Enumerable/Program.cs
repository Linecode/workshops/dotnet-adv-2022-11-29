﻿
var cts = new CancellationTokenSource();

await foreach (int item in RangeAsync(100, 100).WithCancellation(cts.Token))
{
    Console.WriteLine(item + " ");
}

static async IAsyncEnumerable<int> RangeAsync(int start, int count)
{
    for (int i = 0; i < count; i++)
    {
        await Task.Delay(100);
        yield return start+i;
    }
}