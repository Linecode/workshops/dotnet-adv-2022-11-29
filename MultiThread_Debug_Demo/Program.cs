﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

var worker1 = new Thread(StartWorker);
var worker2 = new Thread(StartWorker);
var worker3 = new Thread(StartWorker);

worker1.Start(new WorkerConfig("John", new[] { 1000, 2000 , 3000}));
worker2.Start(new WorkerConfig("Mark", new[] { 2000, 1000, 3000 }));
worker3.Start(new WorkerConfig("Nick", new[] { 3000, 1000, 3000 }));

worker1.Join();
worker2.Join();
worker3.Join();

static void StartWorker(object workerConfig)
{
    var config = (WorkerConfig)workerConfig;

    Thread.CurrentThread.Name = config.Name;

    while(true)
    {
        Console.WriteLine($"Worker {config.Name} sleeps");
        Thread.Sleep(config.TimeSplit[0]);
        Console.WriteLine($"Worker {config.Name} starts work");
        Thread.Sleep(config.TimeSplit[1]);
        Console.WriteLine($"Worker {config.Name} ends work");
        Console.WriteLine($"Worker {config.Name} starts free Time");
        Thread.Sleep(config.TimeSplit[2]);
    }
}

record WorkerConfig(string Name, int[] TimeSplit);