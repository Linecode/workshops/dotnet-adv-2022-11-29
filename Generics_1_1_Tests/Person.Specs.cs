using FluentAssertions;
using Generics_1_1;
using Xunit;

namespace Generics_1_1_Tests
{
    public class Person_Specs
    {
        [Fact]
        public void Person_Should_Have_Default_Name()
        {
            // Arranage
            // Act
            var person = new Person();
            // Arrange
            person.FirstName.Should().Be("John");
        }

        [Theory]
        [InlineData("John")]
        [InlineData("Kamil")]
        [InlineData("Marek")]
        public void Person_FirstName_Should_Be_Assignable(string name)
        {
            // Arranage
            var person = new Person();
            // Act
            person.SetFirstName(name);
            // Assert
            person.FirstName.Should().Be(name);
        }
    }
}