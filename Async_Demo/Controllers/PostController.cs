using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Async_Demo.Controllers
{
    [ApiController]
    public class PostController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;

        public PostController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        // dotnet-counters monitor -p {PID}
        // ab -n 1000 -c 10 https://localhost:5001/async/
        [HttpGet("async")]
        public async Task<IActionResult> GetAsync()
        {
            var client = _clientFactory.CreateClient();
            var response = await client.GetStringAsync($"https://jsonplaceholder.typicode.com/posts/1");
            return Ok(response);
        }
        
        // sync over async
        [HttpGet("sync")]
        public IActionResult GetSync()
        {
            var client = _clientFactory.CreateClient();
            var response = client.GetStringAsync($"https://jsonplaceholder.typicode.com/posts/1").Result;
            return Ok(response);
        }
    }
}