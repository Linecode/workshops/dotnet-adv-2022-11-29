﻿using System.Text.Json;
using System.Text.Json.Serialization;

var cts = new CancellationTokenSource();
using var client = new HttpClient();
var url = "https://jsonplaceholder.typicode.com/todos/1";

var response = await client.GetAsync(url, cts.Token);
var @string = await response.Content.ReadAsStringAsync();

var obj = JsonSerializer.Deserialize<Todo>(@string);

Console.ReadLine();

public record Todo(
    [property:JsonPropertyName("userId")]int UserId, 
    [property:JsonPropertyName("id")]int Id, 
    [property:JsonPropertyName("title")]string Title, 
    [property:JsonPropertyName("completed")]bool Completed);