using System;

namespace Extensions_1_1
{
    public class Result
    {
        public bool IsSuccessful { get; }
        public Exception Exception { get; }

        private protected Result()
        {
            IsSuccessful = true;
        }

        private protected Result(Exception error)
        {
            IsSuccessful = false;
            Exception = error;
        }

        public static Result Success() => new ();
        public static Result Error(Exception error) => new (error);
    }
}