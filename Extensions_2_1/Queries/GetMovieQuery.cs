using System;
using Extensions_1_1.Domain;

namespace Extensions_1_1.Queries
{
    public class GetMovieQuery : IQuery<Result<Movie>>
    {
        public Guid MovieId { get; }

        public GetMovieQuery(Guid movieId)
        {
            MovieId = movieId;
        }
    }
}