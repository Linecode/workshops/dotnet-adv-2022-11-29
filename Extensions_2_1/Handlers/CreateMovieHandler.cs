using Extensions_1_1.Commands;
using Extensions_1_1.Domain;
using Extensions_1_1.Infrastructure;

namespace Extensions_1_1.Handlers
{
    public class CreateMovieHandler : ICommandHandler<CreateMovieCommand>
    {
        private readonly Repository _repository;

        public CreateMovieHandler(Repository repository)
        {
            _repository = repository;
        }
        
        public void Handle(CreateMovieCommand command)
        {
            // command.BeforeHandle?.Invoke(command);
            
            var movie = new Movie
            {
                Id = command.Id,
                Cast = command.Cast,
                Director = command.Director,
                Title = command.Title
            };
            
            _repository.Add(movie);
        }
    }
}