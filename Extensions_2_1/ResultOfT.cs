using System;

namespace Extensions_1_1
{
    public class Result<T> : Result
    {
        private T Data { get; }
        
        private Result(T data)
        {
            Data = data;
        }

        private Result(Exception e) : base(e)
        { }

        public TResult Match<TResult>(Func<T, TResult> OnSuccess, Func<Exception, TResult> OnError)
        {
            if (IsSuccessful) return OnSuccess(Data);
            return OnError(Exception);
        }

        public static Result<T> Success(T data) => new (data);
        public new static Result<T> Error(Exception error) => new (error);
    }
}