# Extensions 2_1

Napisz generyczna metodę rozszerzającą, która tworzy pozytywny rezultat Result<T> z dowolnego typu.

Wymagania: 

- Metoda jest generyczna
- Metoda zwraca Result<T>
- zmodyfikuj implementację metody `Handle` w klasie `GetMovieQuery` aby wykorzystać nowo powstałe rozszerzenie.