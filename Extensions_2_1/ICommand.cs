namespace Extensions_1_1
{
    public interface ICommand
    {
    }

    public interface ICommandHandler<T> where T : ICommand
    {
        public void Handle(T command);
    }
}