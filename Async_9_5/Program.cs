﻿Action dummyAction = async () =>
{
    await Task.Delay(1000);
    await AsyncConsole("No No No No No No No No No No No No No No No No No No!");
};

var t = AwaitableMethod(1);

await t;
await t;

// Console.WriteLine(t.Result);

await await Weird();

// Task[] post = new Task[4];
// for (int i = 0; i < post.Length; i++)
//     post[i] = Task.Factory.StartNew(dummyAction);
//
// await Task.WhenAll(post);

Console.WriteLine("Before Test");
Test();
Console.WriteLine("After Test");

Console.ReadKey();

async Task AsyncConsole(string message)
{
    await Task.Delay(500);
    Console.WriteLine(message);
}

async ValueTask<int> AwaitableMethod(int param)
{
    if (param < 0) return 13;
    await Task.Delay(500);
    return param * 2;
}

Task<Task<int>> Weird()
    => Task.FromResult(Task.FromResult(10));


async void Test()
{
    Console.WriteLine("Test before delay");
    await Task.Delay(100);
    Console.WriteLine("Test after delay");
}
