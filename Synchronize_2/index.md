# Zadanie 9_5_2

- Wykorzystując prywityw synchronizacyjny Monitor, spraw aby klasa BackAccount została klasą ThreadSafe
- Pomyśl czy dałbyś radę opakować kod Monitor-a w klasę lub struktruę tak aby zamknąć kod "lock"-a w klauzuli using
wtedy przykładowy kod mógłby wyglądać następująco:

```csharp
using (SomeCustom.Lock(obj, TimeSpan)) {
    // critical section
}
```
