﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Synchronize_2
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var account = new BankAccount();

            var task = new Task[100];
            for (int i = 0; i < 100; i++)
            {
                task[i] = Task.Run(() => { Update(account);});
            }
            
            await Task.WhenAll(task);
            
            Console.WriteLine($"Bank Balance: {account.Get()}");
            
            Console.ReadLine();
        }

        static void Update(BankAccount account)
        {
            account.Deposit(1000);
            account.Withdraw(1000);
        }
    }
    
    class BankAccount
    {
        private static readonly object _padlock = new();
        private int balance;

        public void Deposit(int amount)
        {
            using (TimedLock.Lock(_padlock, TimeSpan.FromSeconds(2)))
            {
                balance += amount;
            }
            Console.WriteLine($"Deposit {amount}, current balance {balance}");
        }

        public void Withdraw(int amount)
        {
            using (TimedLock.Lock(_padlock, TimeSpan.FromSeconds(2)))
            {
                balance -= amount;
            }

            Console.WriteLine($"Withdraw {amount}, current balance {balance}");
        }

        public int Get()
        {
            using (TimedLock.Lock(_padlock, TimeSpan.FromSeconds(2)))
            {
                return balance;
            }
        }
    }
    
    public ref struct TimedLock
    {
        private object _target;

        public TimedLock(object target)
        {
            _target = target;
        }

        public static TimedLock Lock(object o, TimeSpan timeout)
        {
            TimedLock t = new TimedLock(o);

            if (!Monitor.TryEnter(o, timeout))
            {
                throw new TimeoutException();
            }

            return t;
        }

        public void Dispose() => Monitor.Exit(_target);
    }
}