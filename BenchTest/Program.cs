﻿// See https://aka.ms/new-console-template for more information


using System.Security.Cryptography;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace BenchTet;

[MemoryDiagnoser]
public class Md5Sha256
{
    private const int N = 10000;
    private readonly byte[] data;
    
    private readonly SHA256 sha256 = SHA256.Create();
    private readonly MD5 md5 = MD5.Create();

    public Md5Sha256()
    {
        data = new byte[N];
        new Random(42).NextBytes(data);
    }

    [Benchmark]
    public byte[] Sha256() => sha256.ComputeHash(data);
    
    [Benchmark]
    public byte[] Md5() => md5.ComputeHash(data);
}

public class Program
{
    public static void Main(string[] args)
    {
        var summary = BenchmarkRunner.Run<Md5Sha256>();
    }
}