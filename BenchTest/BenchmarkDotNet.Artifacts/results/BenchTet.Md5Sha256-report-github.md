``` ini

BenchmarkDotNet=v0.13.2, OS=macOS Monterey 12.4 (21F79) [Darwin 21.5.0]
Apple M1 Max, 1 CPU, 10 logical and 10 physical cores
.NET SDK=6.0.301
  [Host]     : .NET 6.0.6 (6.0.622.26707), Arm64 RyuJIT AdvSIMD
  DefaultJob : .NET 6.0.6 (6.0.622.26707), Arm64 RyuJIT AdvSIMD


```
| Method |      Mean |     Error |    StdDev |   Gen0 | Allocated |
|------- |----------:|----------:|----------:|-------:|----------:|
| Sha256 |  4.385 μs | 0.0068 μs | 0.0064 μs | 0.0534 |     112 B |
|    Md5 | 19.265 μs | 0.0400 μs | 0.0355 μs | 0.0305 |      80 B |
