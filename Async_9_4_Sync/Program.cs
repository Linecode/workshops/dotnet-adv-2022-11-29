﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Async_9_4
{
    class Program
    {
        static HttpClient _client = new();
        static SemaphoreSlim _semaphoregate = new(3, 3);  
        
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            Task.WaitAll(CallOtherApi().ToArray());
        }

        public static IEnumerable<Task> CallOtherApi()
        {
            for (int i = 0; i < 100; i++)
            {
                yield return CallApi(i);
            }
        }

        private static async Task CallApi(int id)
        {
            await _semaphoregate.WaitAsync();
            int statusCode = 0;
            try
            {
                var response = await _client.GetAsync("https://example.com");
                statusCode = (int)response.StatusCode;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _semaphoregate.Release();
            }
            
            Console.WriteLine($"[{id}] Status: {statusCode}");
        }
    }
}