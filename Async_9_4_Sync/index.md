# Zadanie 9_4

Wykorzystując zainicjalizowane obiekty HttpClient i SemaphoreSlim napisz kod który:
- wygeneruje 100 Tasków
- każdy task będzie miał za zadanie
  - poczekać na otrzymanie dostępu do kodu za pomocą funkcji Wait z semafora
  - Wysłać żądanie http na domenę example.com
  - opuścić semafor za pomocą metody Release
  - Wypisać na konsoli status żądania http
- Przetestuj jak będzie się zachowywać aplikacja w przypadku innych ustawień semafora np. 5, 10