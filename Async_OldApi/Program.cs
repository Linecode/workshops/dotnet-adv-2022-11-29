﻿
var path = "/Users/kamilkielbasa/Workspace/__linecode/__workshops/dotnet-adv-2022-11-29";
var files = await GetFilesAsync(path);

foreach (var file in files)
{
    Console.WriteLine(file);
}

static Task<string[]> GetFilesAsync(string path)
{
    var tsc = new TaskCompletionSource<string[]>();
    var thread = new Thread(() =>
    {
        Thread.Sleep(2000);
        try
        {
            tsc.TrySetResult(Directory.GetFiles(path));
        }
        catch (Exception e)
        {
            tsc.TrySetException(e);
        }
    });
    
    thread.Start();
    return tsc.Task;
}

