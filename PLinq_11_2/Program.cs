﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace PLinq_11_2
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var cq = new ConcurrentQueue<Action<int>>();

            Action<int> workload = async (id) =>
            {
                await Task.Delay(100);
                Console.WriteLine($"Action handled by task: {id}");
            };
            
            cq.Enqueue(workload);

            await Task.Run(() =>
            {
                for (int i = 0; i < 500; i++)
                {
                    cq.Enqueue(workload);
                }
                
                Console.WriteLine("Added workloads!");
            });
            
            // .... implementacja
            
            Action consumer = () =>
            {
                while (cq.TryDequeue(out var task))
                {
                    task(Task.CurrentId ?? 0);
                }
            };
            
            var tasks = new Task[4];
            for (int i = 0; i < tasks.Length; i++)
                tasks[i] = Task.Factory.StartNew(consumer);
            
            await Task.WhenAll(tasks);
            
            Console.ReadLine();
        }
    }
}