# Zadanie 9_3

- Napisz kod, który wykorzystując klienta Http, do pobrania i zsumowania wagi stron znajdujących się w tablicy urlList.
- Wykorzystaj do tego składnie async / await
- Nastepnie dodaj wykorzystanie cancellation token tak aby ubić proces w momencie kiedy zostanie przekroczony czas 3,5s

Aby pobrac rozmiar storny możesz użyc poniższej metody: 

```csharp
static async Task<int> ProcessUrlAsync(string url, HttpClient client, CancellationToken token)
{
    var response = await client.GetAsync(url, token);
    var content = await response.Content.ReadAsByteArrayAsync(token);
    
    Console.WriteLine($"{url,-60} {content.Length,10:#,#}");

    return content.Length;
}
```