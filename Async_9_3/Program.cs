﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Async_9_3
{
    class Program
    {
        static readonly HttpClient _client = new()
        {
            MaxResponseContentBufferSize = 1_000_000
        };
        
        static async Task Main(string[] args)
        {
            using var cts = new CancellationTokenSource();
            
            cts.CancelAfter(3500);
            await SumPages(cts.Token);

            Console.ReadLine();
        }

        static async Task SumPages(CancellationToken token = default)
        {
            int total = 0;

            foreach (var url in UrlList)
            {
                if (token.IsCancellationRequested)
                    break;

                try
                {
                    int contentLength = await ProcessUrlAsync(url, _client, token);
                    total += contentLength;
                }
                catch (OperationCanceledException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Task Cancelled Due to Timeout! ");
                    break;
                }
            }
            
            Console.WriteLine($"{total} bytes");
        }
        
        static async Task<int> ProcessUrlAsync(string url, HttpClient client, CancellationToken token)
        {
            var response = await client.GetAsync(url, token);
            var content = await response.Content.ReadAsByteArrayAsync(token);
            
            Console.WriteLine($"{url,-60} {content.Length,10:#,#}");

            return content.Length;
        }
        
        static readonly IEnumerable<string> UrlList = new string[]
        {
            "https://docs.microsoft.com",
            "https://docs.microsoft.com/aspnet/core",
            "https://docs.microsoft.com/azure",
            "https://docs.microsoft.com/azure/devops",
            "https://docs.microsoft.com/dotnet",
            "https://docs.microsoft.com/dynamics365",
            "https://docs.microsoft.com/education",
            "https://docs.microsoft.com/enterprise-mobility-security",
            "https://docs.microsoft.com/gaming",
            "https://docs.microsoft.com/graph",
            "https://docs.microsoft.com/microsoft-365",
            "https://docs.microsoft.com/office",
            "https://docs.microsoft.com/powershell",
            "https://docs.microsoft.com/sql",
            "https://docs.microsoft.com/surface",
            "https://docs.microsoft.com/system-center",
            "https://docs.microsoft.com/visualstudio",
            "https://docs.microsoft.com/windows",
            "https://docs.microsoft.com/xamarin"
        };
    }
}