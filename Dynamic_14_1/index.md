# Zadanie 14_1

Napisz metodę rozszerzającą, która rozszerzy interfejs IEnumerable<T> o nazwie ShapeData:
- Metoda ma być metodą generyczną
- Metoda ma zwracać IEnumerable<ExpandoObject>
- Metoda ma przyjmować parametr string fields
- W metodzie zaimplementuj mechanizm konwersji tej kolekcji na typ ExpandoObject, w którym znajdą się propertisy zdefiniowane w zmiennej fields
- Wywołanie tej metody ma wyglądać następująco: `someCollection.ShapeData("someProps, otherProps")`
