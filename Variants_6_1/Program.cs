﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Variants_6_1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee> {  
                new() {FirstName = "Michael", LastName = "Alexander"},  
                new() {FirstName = "Jeff", LastName = "Price"}  
            };
            
            // Covariance
            PrintFullName(employees);
            
            // Contravariant
            IEnumerable<Employee> noDuplicates = employees.Distinct<Employee>(new PersonComparer());

            // No contravariant
            IEnumerable<Employee> no = employees.Select(x => x as Person)
                .Distinct(new PersonComparer()).Select(x => x as Employee);

            PrintFullName(noDuplicates);
        }
        
        public static void PrintFullName(IEnumerable<Person> persons)
        {
            foreach (var person in persons)
            {
                Console.WriteLine("Name: {0} {1}", person.FirstName, person.LastName);
            }
        }
    }
    
    public class Person  
    {  
        public string FirstName { get; set; }  
        public string LastName { get; set; }  
    }  
  
    public class Employee : Person { }

    public class PersonComparer : IEqualityComparer<Person>
    {
        public bool Equals(Person x, Person y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.FirstName == y.FirstName && x.LastName == y.LastName;
        }

        public int GetHashCode(Person obj)
        {
            return HashCode.Combine(obj.FirstName, obj.LastName);
        }
    }
}