﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Async_Playground
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // for (int i = 0; i < 10; i++)
            // {
            //     int temp = i;
            //     Task.Run(() => { Console.WriteLine(temp); });
            // }

            TaskScheduler.UnobservedTaskException += (sender, eventArgs) =>
            {
                Console.WriteLine("unhandled task error");
                eventArgs.SetObserved();
            };

            // try
            // {
            //     Task.Run(() => { throw new Exception(""); }).Wait();
            // }
            // catch (Exception ex)
            // {
            //     Console.WriteLine(ex.InnerException.Message);
            // }
            //
            // Task<int> response = Task.Run(() => { return 1; });
            //
            // Console.WriteLine($"{response.Result}");

            // DumpThread("Before");
            //
            // await Test();
            //
            // DumpThread("After");
            //
            // await 1;
            //
            // await 1000;
            //
            // await 66666;
            //
            // await TimeSpan.FromSeconds(1);
            //
            // var t = Task.Run(async () =>
            // {
            //     await Task.Delay(1000);
            //     return "Hello";
            // });
            //
            // t.ContinueWith(async (x) =>
            // {
            //     var result = x.Result;
            //     Console.WriteLine($"{result}");
            //
            //     await Task.Delay(1000);
            // }).Wait();

            // var task = Method();
            // // ------------------------
            // await task;
            // await task;
            //
            // await await Method2();
            //

            // var a = Method();
            // var b =  Method2();
            // var c=  Method3();
            //
            // await Task.WhenAll(a, b, c);
            //
            // var d = a.Result;
            //
            // var e = Method3();
            // var f = Timeout();
            //
            // await Task.WhenAny(e,f);
            //
            // Task.WaitAll();
            // Task.WaitAny();
            //
            
            // var parent = Task.Factory.StartNew((async () =>
            // {
            //     Console.WriteLine("Running parent task");
            //
            //     var child = Task.Factory.StartNew(() =>
            //     {
            //         Console.WriteLine("Running child started");
            //         Thread.Sleep(1000);
            //         Console.WriteLine("Running child finished");
            //     });
            //
            //     await child;
            //     
            //     Console.WriteLine("Parent Completed");
            // }));
            //
            // parent.Wait();

            // Action<int> action = async (id) =>
            // {
            //     await Task.Delay(1000);
            //     Console.WriteLine(id);
            // };
            //
            // var tasks = new Task[100];
            // for (int i = 0; i < tasks.Length; i++)
            //     tasks[i] = Task.Factory.StartNew((x) => { action((int)x!); }, i);

            // var tasks = Enumerable.Range(0, 10)
            //     .Select(x =>
            //     {
            //         Console.WriteLine(x);
            //         return Task.Delay(3000);
            //     }).ToList();
            //
            // foreach (var task in tasks)
            // {
            //     await task;
            // }
            //
            // var a = IsOver18(18);
            //
            // a.AsTask();

            await using var a = new Smth();
            a.Test();

            Console.ReadLine();
        }

        public static async Task Timeout()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
        }

        public static async Task Test()
        {
            await Task.Delay(1000);
        }

        static void DumpThread(string label)
        {
            Console.WriteLine($"[{DateTime.Now:hh:mm:ss.fff}] {label}: " +
                              $"ID: {Thread.CurrentThread.ManagedThreadId} " +
                              $"IsOnThreadPool: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"IsBackground: {Thread.CurrentThread.IsBackground}");
        }

        static Task<int> Method()
        {
            return Task.FromResult(1);
        }

        static Task<Task> Method2()
        {
            return Task.FromResult(Task.CompletedTask);
        }

        static Task<int> Method3()
        {
            return Task.FromResult(10);
        }

        static async ValueTask<int> IsOver18(int age)
        {
            if (age < 18) return 0;
            
            return await Method3();
        }
    }
    
    public static class IntExtensions
    {
        public static TaskAwaiter<int> GetAwaiter(this int number)
            => Task.FromResult(number).GetAwaiter();
    }

    public static class TimeSpanExtensions
    {
        public static TaskAwaiter GetAwaiter(this TimeSpan timeSpan)
            => Task.Delay(timeSpan).GetAwaiter();
    }

    public class Smth : IAsyncDisposable
    {
        public void Test()
        {
            
        }
        
        public async ValueTask DisposeAsync()
        {
            await Task.Delay(1000);
            Console.WriteLine("Disposed");
        }
    }
}