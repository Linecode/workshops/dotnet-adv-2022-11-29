# Generics 1_1

Twoim zadaniem będzie zaimplementowanie dwóch klas: 
- Result -> klasa bazowa, która będzie zawierać informację czy dana operacja zakończyła się powodzeniem
- Result<T> -> klasa dziedzicąca po klasie bazowej Result, która oprócz przechowywania wyniku operacji, przechowuje również dane zwrócone przez operację

Klasa Result powinna się składać z takich elementów jak:
- Prywatny konstruktor bezparametrowy
- Prywatny konstruktor pozwalający na dodanie Exception
- Statycznej metody pomocniczej `Success` tworzącej obiekt result z pozytywnym wynikiem operacji
- Statycznej metody pomocniczej `Error` tworzącej instancje obiektu Result z przekazanym błędem (Exception)

Klasa Result<T> powinna: 
- Znajdować się w osobnym pliku nazwanym `ResultOfT`
- Posiadać prywatny konstruktor pozwalający na ustawienie danych zwróconych przez operację
- Posiadać prywatny konstruktor pozwalający na dodanie Exception
- Statycznej metody pomocniczej `Success` tworzącej obiekt result z pozytywnym wynikiem operacji i przekazanymi danymi
- Statycznej metody pomocniczej `Error` tworzącej instancje obiektu Result z przekazanym błędem (Exception)

- Na koniec uruchom testy z projektu `Generics_1_1_Tests` aby sprawdzić czy wszystko działa poprawnie.

```mermaid
classDiagram
class Result {
    +Exception Error
    +Bool IsSuccess
    -Result()
    -Result(Exception error)
    +static Success() Result
    +static Error(Exception error) Result
}
```

```mermaid
classDiagram
class Result~T~ {
    +Exception Error
    +Bool IsSuccess
    +T Data
    -Result(T Data)
    -Result(Exception error)
    +static Success(T data) Result~T~
    +static Error(Exception error) Result~T~
}
```