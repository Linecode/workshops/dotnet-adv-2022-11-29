using System;

namespace Generics_1_1
{
    public class Result
    {
        public bool IsSuccessful { get; protected set; }
        public Exception Exception { get; protected set; }
        // public object Data { get; protected set; }

        private protected Result()
        {
            IsSuccessful = true;
        }

        private protected Result(Exception exception)
        {
            IsSuccessful = false;
            Exception = exception;
        }

        // private protected Result(object data)
        // {
        //     IsSuccessful = true;
        //     Data = data;
        // }

        public static Result Success()
        {
            return new Result();
        }

        // public static Result Success(object data)
        // {
        //     return new Result(data);
        // }

        public static Result Error(Exception exception)
        {
            return new Result(exception);
        }
    }
}

// Result.Success();
// Result.Failure(new Exception());

// Result.Success((object)int)
// var result = result.Data as int;