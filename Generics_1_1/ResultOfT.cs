using System;

namespace Generics_1_1
{
    public class Result<T> : Result
    {
        public T Data { get; }

        private Result(T data)
        {
            Data = data;
            IsSuccessful = true;
        }

        private Result(Exception exception)
        {
            IsSuccessful = false;
            Exception = exception;
        }

        public static Result<T> Success(T data)
        {
            return new Result<T>(data);
        }

        public static Result<T> Error(Exception exception)
        {
            return new Result<T>(exception);
        }
    }
}

// Result<int>.Success(10);
// var a = result.Data;