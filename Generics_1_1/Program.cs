﻿using System;

namespace Generics_1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var someService = new SomeService();

            var a = someService.Get(Guid.NewGuid()).Data;

            Console.WriteLine("Hello World!");
        }
    }
}