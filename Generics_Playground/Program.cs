﻿using System;

namespace Generics_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // var counter = new GenericCounter<string>();
            // var counteri = new GenericCounter<int>();
            //
            // counter.Increment();
            // counter.Increment();
            // counter.Increment();
            // counter.Display();
            //
            // counteri.Increment();
            // counteri.Increment();
            // counteri.Display();

            // var a = A.Test<string>();
            // var b = A.Test<int>();
            // var c = A.Test<C>();
            //
            // Console.WriteLine(a);
            // Console.WriteLine(b);
            // Console.WriteLine(c is null);
            //
            // var d = new DataSet<C>();
            // var e = d.GetData();

            var a = new D();
            Console.WriteLine(a.ToString());
        }
    }

    class GenericCounter<T>
    {
        private int _value;

        public  void Increment()
        {
            _value++;
        }

        public void Display()
        {
            Console.WriteLine("Counter for {0}:{1}", typeof(T), _value);
        }
    }

    public class A
    {
        public static T Test<T>()
        {
            return default(T);
        }
    }
    
    public class C {}

    public class DataSet<T> where T : class, new()
    {
        private T _dataSet;
        
        public bool HasValue { get; private set; } = false;

        public void SetData(T data)
        {
            _dataSet = data;
            HasValue = true;
        }

        public T GetData()
        {
            if (HasValue) return _dataSet;

            return new T();
        }
    }
    
    public partial class D
    {
        public override string ToString()
        {
            string text = "original";
            CustomizeText(ref text);
            return text;
        }
        partial void CustomizeText(ref string text);
    }
    
    public partial class D
    {
        partial void CustomizeText(ref string text)
        {
            text += "customize";
        }
    }
}
