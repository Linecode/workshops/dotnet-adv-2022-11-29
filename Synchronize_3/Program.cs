﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nito.AsyncEx;

namespace Synchronize_3
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            var account = new BankAccount();
            var task = new Task[100];
            for (int i = 0; i < 100; i++)
            {
                task[i] = Task.Run(async () => { await Update(account);});
            }
            
            await Task.WhenAll(task);
            
            Console.WriteLine($"Bank Balance: {await account.Get()}");
            
            Console.ReadLine();
        }

        static async Task Update(BankAccount account)
        {
            await account.Deposit(1000);
            await account.Withdraw(1000);
        }
    }
    
    class BankAccount
    {
        private AsyncLock _lock = new();
        private int balance;

        public async Task Deposit(int amount)
        {
            using (await _lock.LockAsync())
            {
                await Task.Delay(100);
                balance += amount;
            }
            Console.WriteLine($"Deposit {amount}, current balance {balance}");
        }

        public async Task Withdraw(int amount)
        {
            using (await _lock.LockAsync())
            {
                await Task.Delay(100);
                balance -= amount; 
            }
            Console.WriteLine($"Withdraw {amount}, current balance {balance}");
        }

        public async Task<int> Get()
        {
            using (await _lock.LockAsync())
            {
                await Task.Delay(100);
            }

            return balance;
        }
    }
}