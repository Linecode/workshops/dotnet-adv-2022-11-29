﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Microsoft.VisualBasic;

namespace Collections_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // #region System.Collections
            // var arrList = new ArrayList();
            //
            // arrList.Add(1);
            // arrList.Add(2);
            // arrList.Add("3");
            // arrList.Add("6");
            //
            // var hash = new Hashtable();
            // hash.Add(1, "one");
            // hash.Add("two", 1);
            //
            // var queue = new Queue();
            // queue.Enqueue(1);
            // queue.Enqueue(2);
            //
            // queue.Dequeue();
            //
            // var stack = new Stack();
            // stack.Push(1);
            // stack.Push(2);
            //
            // stack.Pop();
            // #endregion
            //
            // #region System.Collections.Generics
            //
            // IEnumerable<string> list = new List<string>
            // {
            //     "one",
            //     "two",
            //     "three"
            // };
            //
            // foreach (var s in list)
            // {
            //     Console.WriteLine(s);
            // }
            //
            // var con = new Collection<Person>();
            //
            // var dictionary = new Dictionary<string, object>();
            //
            // dictionary.Add("one", "one");
            // dictionary.Add("two", new {});
            //
            // var input = "upper";
            //
            // var actions = new Dictionary<string, Func<string, string>>
            // {
            //     { "upper", x => x.ToUpper() },
            //     { "lower", x => x.ToLower() }
            // };
            //
            // bool found = actions.TryGetValue(input, out var handler);
            //
            // if (found)
            // {
            //     var result = handler(input);
            //     Console.WriteLine(result);
            // }

            // var queue = new PriorityQueue<string, int>();
            // queue.Enqueue("one", 78);
            // queue.Enqueue("two", 2);
            // queue.Enqueue("two2", 2);
            // queue.Enqueue("two3", 2);
            // queue.Enqueue("three", 60);
            //
            // while (queue.TryDequeue(out var item, out var priority))
            // {
            //     Console.WriteLine($"Popped item: {item} with priority: {priority}");
            // }
            
            // var queue = new PriorityQueue<string, string>(new TitleComparer());
            // queue.Enqueue("Kamil Kiełbasa", "Mr");
            // queue.Enqueue("Ktos", "Sir");
            // queue.Enqueue("dsdas", "Mr");
            // queue.Enqueue("lmlmlmlm", "Sir");
            //
            // while (queue.TryDequeue(out var item, out var priority))
            // {
            //     Console.WriteLine($"Popped item: {item} with priority: {priority}");
            // }

            var queue = new PriorityQueue<Func<string>, int>();
            queue.Enqueue(() => "a", 100);
            queue.Enqueue(() => "z", 10);
            queue.Enqueue(() => "h", 50);
            queue.Enqueue(() => "i", 1);
            
            while (queue.TryDequeue(out var item, out var priority))
            {
                Console.WriteLine($"Popped item: {item()} with priority: {priority}");
            }
            
            // #endregion
            var personsList = new PersonList();

            var person = personsList[1];
        }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        private readonly List<string> _credentials = new();
        public IReadOnlyList<string> Credentials => _credentials;

        public Person()
        {
        }

        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
    }

    public class TitleComparer : IComparer<string>
    {
        public int Compare(string a, string b)
        {
            var titleAIsFancy = a.Equals("Sir", StringComparison.InvariantCulture);
            var titleBIsFancy = b.Equals("Sir", StringComparison.InvariantCulture);

            if (titleAIsFancy && titleBIsFancy)
                return 0;
            
            if (titleAIsFancy)
                return -1;
            
            return 1;
        }
    }

    public class PersonList : IEnumerable
    {
        private readonly List<Person> _list = new()
        {
            new Person("John", "Doe")
        };
        
        public IEnumerator GetEnumerator()
        {
            return new PersonEnumerator(_list.ToArray());
        }

        public Person this[int i]
        {
            get => _list.ElementAt(i);
            set => _list[i] = value;
        }

        public void Add(Person person)
            => _list.Add(person);
        
        private class PersonEnumerator : IEnumerator
        {
            private readonly Person[] _persons;
            private int _position = -1;

            public PersonEnumerator(Person[] persons)
            {
                _persons = persons;
            }
            
            public bool MoveNext()
            {
                _position++;
                return (_position < _persons.Length);
            }

            public void Reset()
            {
                _position = -1;
            }

            public object Current => _persons[_position];
        }
    }
}