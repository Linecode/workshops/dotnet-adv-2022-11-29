using FluentAssertions;
using UnitTests_Playground;
using Xunit;

namespace UnitTests_Tests;

public class SomeEntity_Tests
{
    [Fact]
    public void Test()
    {
        // AAA
        //   Arrange
        //   Act
        //   Assert
        // GWT
        //   Given
        //   When
        //   Then
        
        // Arrange
        var someEntity = new SomeEntity
        {
            Name = "A",
            Name2 = "B",
        };

        // Act
        var result = someEntity.Concat;
        
        // Assert
        Assert.Equal("A B", result);
    }

    [Theory]
    [InlineData("", 0)]
    [InlineData("a", 1)]
    [InlineData("aa", 2)]
    [InlineData(null, 0)]
    public void ToTest_Should_Return_Param_Length(string name, int expected)
    {
        // Arrange
        var someEntity = new SomeEntity();
        
        // Act
        var result = someEntity.ToTest(name);
        
        // Assert
        Assert.Equal(expected, result);
    }
    
    /// TDD
    /// Test Driven Development
    /// - Napisanie testu -> Piszemy impementacje -> Refactor -> Napisanie testu -> Piszemy impementacje -> Refactor
    ///
    ///
    /// Co_Testuje_SpodziewanyWynik_Założenie()
    /// Apply_Should_Apply_Envent_When_Even_Is_Valid
    [Theory]
    [InlineData(10, 100)]
    [InlineData(5, 25)]
    public void ToTest_2_Should_Return_Valid_Result(int param, int expectedValue)
    {
        // Arrange
        var someEntity = new SomeEntity();

        // Act
        var result = someEntity.ToTest2(param);
        
        // Assert
        Assert.Equal(expectedValue, result);
        
        // BDD - Behviour Driven Development
        result.Should().Be(expectedValue);
    }
}
