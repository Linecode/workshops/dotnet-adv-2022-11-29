using FluentAssertions;
using UnitTests_Playground;
using Xunit;

namespace UnitTests_Tests;

public class Person_Test
{
    [Theory]
    [InlineData(18)]
    [InlineData(19)]
    [InlineData(90)]
    public void IsOver18_Should_Return_True_For_Values_GthOrEq_Than_18(int age)
    {
        // Arrange
        var person = new Person("", "",age);

        // Act
        var result = person.IsOver18();

        // Assert
        result.Should().BeTrue();
    }

    [Theory]
    [InlineData(17)]
    [InlineData(10)]
    public void IsOver18_Should_Return_False_For_Values_Less_Than_18(int age)
    {
        // Arrange
        var person = new Person("", "",age);

        // Act
        var result = person.IsOver18();

        // Assert
        result.Should().BeFalse();
    }

    [Theory]
    [InlineData("", "", false)]
    [InlineData("dsadsa", "", false)]
    [InlineData("", "dsadsadsa", false)]
    [InlineData("a", "a", true)]
    public void AreNamesValid_Should_Return_Excepted_Value(string name, string lastName, bool expected)
    {
        // Arrange
        var person = new Person(name, lastName, 10);
        
        // Act
        var result = person.AreNamesValid();

        // Assert
        result.Should().Be(expected);
    }

    [Theory]
    [InlineData(null, "sas")]
    [InlineData("dsadsa", null)]
    [InlineData(null, null)]
    public void Person_Should_Thrown_Argument_Null_Exception_When_Null_Is_Provided_As_A_Name(string name,
        string lastName)
    {
        // Arrange & Act
        var result = Record.Exception(() =>
        {
            new Person(name, lastName, 10);
        });
        
        // Assert
        result.Should().NotBeNull()
            .And.BeOfType<ArgumentNullException>();
    }
}