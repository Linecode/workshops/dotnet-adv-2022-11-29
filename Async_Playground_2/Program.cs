﻿// See https://aka.ms/new-console-template for more information

class Program
{
    static ThreadLocal<string> _threadLocalString = new();
    static AsyncLocal<string> _asyncLocalString = new();
    public static async Task Main(string[] args)
    {
        await AsyncMethodA();
    }

    static async Task AsyncMethodA()
    {
        _threadLocalString.Value = "Value 1";
        _asyncLocalString.Value = "Value 1";
        var t1 = AsyncMethodB("Value 1");
        
        _threadLocalString.Value = "Value 2";
        _asyncLocalString.Value = "Value 2";
        var t2 = AsyncMethodB("Value 2").ConfigureAwait(false);

        await t1;
        await t2;
    }

    static async Task AsyncMethodB(string expectedValue)
    {
        Console.WriteLine("Entering method B");
        Console.WriteLine("Thread Local Value: expected: {0} real; {1}", expectedValue,_threadLocalString.Value);
        Console.WriteLine("Async Local Value: expected: {0} real; {1}", expectedValue, _asyncLocalString.Value);
        await Task.Delay(1000);
        Console.WriteLine("Thread Local Value: expected: {0} real; {1}", expectedValue,_threadLocalString.Value);
        Console.WriteLine("Async Local Value: expected: {0} real; {1}", expectedValue, _asyncLocalString.Value);
    }
}