﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Synchronize_1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var account = new BankAccount();
            var task = new Task[100];
            for (int i = 0; i < 100; i++)
            {
                task[i] = Task.Run(() => { Update(account);});
            }
            
            await Task.WhenAll(task);
            
            Console.WriteLine($"Bank Balance: {account.Get()}");
            
            Console.ReadLine();
        }

        static void Update(BankAccount account)
        {
            account.Deposit(1000);
            account.Withdraw(1000);
        }
    }

    class BankAccount
    {
        private readonly object _padlock = new();
        private int _balance;

        public void Deposit(int amount)
        {
            lock (_padlock)
            {
                _balance += amount;
            }
            Console.WriteLine($"Deposit {amount}, current balance {_balance}");
        }

        public void Withdraw(int amount)
        {
            lock (_padlock)
            {
                _balance -= amount;
            }
            Console.WriteLine($"Withdraw {amount}, current balance {_balance}");
        }

        public int Get()
        {
            lock (_padlock)
            {
                return _balance;
            }
        }
    }
}