﻿using System;
using System.Collections.Generic;
using FluentApi_5_1.Domain;

namespace FluentApi_5_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var movie = new MovieBuilder()
                .DirectedBy.Id(Guid.NewGuid())
                    .FirstName("John")
                    .LastName("Doe")
                .WithTitle("Title Movie")
                .WithId(Guid.NewGuid())
                .Performed.FirstName("dsdsadas").LastName("dsdsadsdsa").Add()
                .Performed.FirstName("dsadsadasdsa").LastName("dsdsadsadas").Add()
                .Build();
        }
    }

    public class CastMemberBuilder : MovieBuilder
    {
        public MovieParty Party = new()
        {
            Role = MoviePartyRole.Actor
        };

        private bool IsCompleted()
        {
            return Party.IsValid();
        }
        
        public CastMemberBuilder(Movie movie)
        {
            _movie = movie;
        }

        public CastMemberBuilder FirstName(string name)
        {
            Party.FirstName = name;
            return this;
        }

        public CastMemberBuilder LastName(string name)
        {
            Party.LastName = name;
            return this;
        }

        public CastMemberBuilder Add()
        {
            if (IsCompleted())
            {
                _movie.Cast.Add(Party);
                return this;
            }

            throw new InvalidOperationException();
        }
    }

    public class DirectorBuilder : MovieBuilder
    {
        private readonly Movie _movie;

        public DirectorBuilder(Movie movie)
        {
            _movie = movie;

            _movie.Director = new MovieParty
            {
                Role = MoviePartyRole.Director
            };
        }

        public DirectorBuilder Id(Guid id)
        {
            _movie.Director.Id = id;
            return this;
        }

        public DirectorBuilder FirstName(string name)
        {
            _movie.Director.FirstName = name;
            return this;
        }

        public DirectorBuilder LastName(string name)
        {
            _movie.Director.LastName = name;
            return this;
        }
    }

    public class MovieBuilder
    {
        protected Movie _movie = new();

        public DirectorBuilder DirectedBy => new(_movie);
        public CastMemberBuilder Performed => new(_movie);

        public MovieBuilder WithTitle(string title)
        {
            _movie.Title = title;
            return this;
        }

        public MovieBuilder WithId(Guid id)
        {
            _movie.Id = id;
            return this;
        }

        public Movie Build()
        {
            return _movie;
        }
    }
}