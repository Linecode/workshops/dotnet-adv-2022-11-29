﻿using System;
using System.Threading.Tasks;

namespace Async_9_0
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var result = await Task.Run(async () =>
            {
                await Task.Delay(1000);
                Console.WriteLine($"{DateTime.UtcNow}");
                return 11;
            });
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}