﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace IoC_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection();

            // services.AddScoped<IDbContext, DbContext>();
            services.Scan(x => x.FromAssemblyOf<Program>()
                .AddClasses(x => x.AssignableTo<IDbContext>())
                .AsImplementedInterfaces()
                .WithScopedLifetime());
            services.AddTransient<PersonRepository>();
            services.AddTransient<PersonService>();
            
            // Transient -> obiekt będzie wytwarzany zawsze
            // Scoped -> obiket będzie tworzony w zakresie scope-a
            // Singleton -> obiekt będzie tworzony raz na instancję provider-a

            var prodiver = services.BuildServiceProvider();

            using var scope2 = prodiver.CreateAsyncScope();
            {
                
            }

            using var scope = prodiver.CreateScope();
            var personService = scope.ServiceProvider.GetService<PersonService>();
            var contexts = scope.ServiceProvider.GetService<IEnumerable<IDbContext>>();

            // var di = new MyDi();
            //
            // di.RegisterType<IDbContext, MongoDbContext>();
            // di.RegisterType<PersonRepository>();
            //
            // var dbContext = di.GetService<IDbContext>();

            // Pure DI
            // var personService = new PersonService(new PersonRepository(DbContextFactory.GetOrCreate()));

        }
    }

    public static class ServiceLocator
    {
        public static Dictionary<Type, Type> MyTypes { get; set; } = new();
        public static void RegisterType<T>()
        {
            MyTypes.Add(typeof(T), typeof(T));
        }
    
        public static void RegisterType<T, T2>()
        {
            MyTypes.Add(typeof(T), typeof(T2));
        }
    
        public static void RegisterType(Type T1, Type T2)
        {
            MyTypes.Add(T1, T2);
        }
    
        public static T GetService<T>()
        {
            var exists = MyTypes.TryGetValue(typeof(T), out var type);
    
            if (!exists)
                throw new Exception("");
            
            return (T)Activator.CreateInstance(type);
        }
    }

    public class PersonService
    {
        private readonly PersonRepository _repository;

        public PersonService(PersonRepository repository)
        {
            _repository = repository;
        }
    }

    public class PersonRepository
    {
        private readonly IDbContext _context;

        public PersonRepository(IDbContext context)
        {
            _context = context;
        }

        public void Smth()
        {
            var dep = ServiceLocator.GetService<IDbContext>();
        }
    }

    public interface IDbContext
    {
        
    }
    
    public class DbContext : IDbContext {
    
    }
    
    public interface IMongoDbContext {}
    
    public class MongoDbContext : IDbContext, IMongoDbContext {}

    public class DbContextFactory
    {
        private static DbContext instance;

        public static DbContext GetOrCreate()
        {
            if (instance == null)
            {
                instance = new DbContext();
            }

            return instance;
        }
    }
}
