using IoC_Microsoft_WebApi.Repositories;
using IoC_Microsoft_WebApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace IoC_Microsoft_WebApi.Controllers
{
    [ApiController]
    public class SomeController : Controller
    {
        private readonly IService2 _service;
        private readonly IRepository _repository;
        private readonly SomeType _someType;

        public SomeController(IService2 service, IRepository repository, SomeType someType)
        {
            _service = service;
            _repository = repository;
            _someType = someType;
        }
        
        // GET
        public IActionResult Index([FromServices] IService service)
        {
            return Ok();
        }
    }
}