# Zadanie 9_1

- Za pomocą api Task.Run, stwórz dwa wątki background.
- Pierwszy z nich niech odlicza co sekundę w dół.
- Natomiast drugi z nich niech odlicza co sekundę w górę.
- W momencie kiedy pierwszy wątek osiągnie liczbę -10 zakończ wątek. Wypisz na konsoli stosowny komunikat.
- W momencie kiedy drugi wątek osiągnie liczbę 15 zakończ wątek. Wypisz na konsoli stosowny komunikat.