﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Async_9_1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var task1 = Task.Run(async () =>
            {
                var i = 0;
                while (i > -10)
                {
                    i--;
                    Console.WriteLine(i);
                    await Task.Delay(1000);
                }
            });
            var task2 = Task.Run(async () =>
            {
                var i = 0;
                while (i < 15)
                {
                    i++;
                    Console.WriteLine(i);
                    await Task.Delay(1000);
                }
            });

            await Task.WhenAll(task1, task2);
        }
        
        static void DumpThreadInfo()
        {
            Console.WriteLine($"Thread: id: {Thread.CurrentThread.ManagedThreadId} " +
                              $"isThreadPoolThread: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"isBackground: {Thread.CurrentThread.IsBackground}");
        }
    }
}