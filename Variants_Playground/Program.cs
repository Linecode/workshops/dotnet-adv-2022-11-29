﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Variants_Playground
{
    class Program
    {
        public static void Main(string[] args)
        {
            var a = new List<string>();

            IEnumerable<string> b = a;
            IEnumerable c = a;

            IContravariant<object> conobject = new Contravariant<object>();
            IContravariant<string> constring = new Contravariant<string>();

            constring = conobject;

            ICovariant<string> costring = new Covariant<string>();
            ICovariant<object> coobject = new Covariant<object>();

            coobject = costring;
        }
    }
    
    public interface IContravariant<in T> {}
    public interface IExtContravariant<in T> : IContravariant<T> {}

    class Contravariant<T> : IContravariant<T>
    { }

    public interface ICovariant<out T> {}

    public interface IExtCovariant<out T> : ICovariant<T> {};

    class Covariant<T> : ICovariant<T>
    {
        
    }

    class Animal {}
    class Dog : Animal {}
    class Cat : Animal {}

    class Pets : IEnumerable<Cat>, IEnumerable<Dog>
    {
        IEnumerator<Dog> IEnumerable<Dog>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator<Cat> IEnumerable<Cat>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}

/// Covariant:
/// From .NEt Framework 4. 0
/// - IEnumerable<T>
/// - IEnumerator<T>
/// - IQueryable<T>
/// - IGrouping<T,K>
///
///
/// from .NET Framework 4,5
/// - IReadOnlyList<T>
/// - IReadonlyCollection<T>
///
///
/// Contravariance:
///  - ICompare<T>
///  - IEqualityComparer<T>
