﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Async_9_2
{
    class Program
    {
        private static int _count = 0;
        static async Task Main(string[] args)
        {
            var tasks = Enumerable.Range(0, 5).Select(taskNr =>
            {
                return PrintTask(taskNr);
            }).ToList();

            foreach (var task in tasks)
            {
                await task;
            }
            
            Console.WriteLine($"The End, random sum {_count}");
            
            static async Task<int> PrintTask(int taskNr)
            {
                Console.WriteLine($"PrintTask tasknr {taskNr}");
                int rnd = new Random().Next(1, 5);
                Console.WriteLine($"PrintTask tasknr {taskNr} rnd {rnd}");
                await Task.Delay(rnd * 1000);
                Interlocked.Add(ref _count, rnd);
                Console.WriteLine($"PrintTask tasknr {taskNr} end");
            
                return rnd;
            }
        }
    }
}