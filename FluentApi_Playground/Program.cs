﻿using System;

namespace FluentApi_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // var address = new AddressBuilder()
            //     .WithCity("Warszawa")
            //     .WithState("Mazowieckie")
            //     .WithStreet("Puławska")
            //     .WithZipCode("02-777")
            //     .Build();

            var person = new PersonBuilder()
                .Smth.WithSmth("dsamll")
                .Lives.At("Puławskiej")
                    .In("Warszawa")
                    .WithZipCode("02-777")
                .WithFirstName("dsadsadas")
                .WithLastName("dsadsadsdsa")
                .WithId(123213231);
        }
    }

    public class PersonObjectMother
    {
        public Person RegularJoe => new PersonBuilder()
            .Smth.WithSmth("dsamll")
            .Lives.At("Puławskiej")
                .In("Warszawa")
                .WithZipCode("02-777")
            .WithFirstName("dsadsadas")
            .WithLastName("dsadsadsdsa")
            .WithId(123213231)
            .Build();
        
        public Person Admin => new PersonBuilder()
            .Smth.WithSmth("dsamll")
            .Lives.At("Puławskiej")
                .In("Warszawa")
                .WithZipCode("02-777")
            .WithFirstName("dsadsadas")
            .WithLastName("dsadsadsdsa")
            .WithId(123213231)
            .Build();
    }

    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Address Address { get; set; }
        public Smth Smth { get; set; }
    }

    public class Address
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }

    public class Smth
    {
        public string Test { get; set; }
    }

    public class SmthBuilder : PersonBuilder
    {
        public SmthBuilder(Person person)
        {
            Person = person;

            Person.Smth = new Smth();
        }

        public SmthBuilder WithSmth(string smth)
        {
            Person.Smth.Test = smth;
            return this;
        }
    }

    public class AddressBuilder : PersonBuilder
    {
        public AddressBuilder(Person person)
        {
            Person = person;

            Person.Address = new Address();
        }

        public AddressBuilder At(string street)
        {
            Person.Address.Street = street;
            return this;
        }

        public AddressBuilder In(string city)
        {
            Person.Address.City = city;
            return this;
        }

        public AddressBuilder WithZipCode(string zipCode)
        {
            Person.Address.ZipCode = zipCode;
            return this;
        }
    }

    public class PersonBuilder
    {
        protected Person Person = new();

        public AddressBuilder Lives => new(Person);
        public SmthBuilder Smth => new(Person);

        public PersonBuilder WithFirstName(string firsName)
        {
            Person.FirstName = firsName;
            return this;
        }

        public PersonBuilder WithLastName(string lastName)
        {
            Person.LastName = lastName;
            return this;
        }

        public PersonBuilder WithId(int id)
        {
            Person.Id = id;
            return this;
        }
        
        public Person Build()
        {
            return Person;
        }
    }

    // public class AddressBuilder
    // {
    //     private string Street;
    //     private string City;
    //     private string State;
    //     private string ZipCode;
    //
    //     public AddressBuilder WithStreet(string street)
    //     {
    //         Street = street;
    //         return this;
    //     }
    //
    //     public AddressBuilder WithCity(string city)
    //     {
    //         City = city;
    //         return this;
    //     }
    //
    //     public AddressBuilder WithState(string state)
    //     {
    //         State = state;
    //         return this;
    //     }
    //
    //     public AddressBuilder WithZipCode(string zipCode)
    //     {
    //         ZipCode = zipCode;
    //         return this;
    //     }
    //
    //     public Address Build()
    //     {
    //         return new Address
    //         {
    //             Street = Street,
    //             City = City,
    //             State = State,
    //             ZipCode = ZipCode
    //         };
    //     }
    // }
}
