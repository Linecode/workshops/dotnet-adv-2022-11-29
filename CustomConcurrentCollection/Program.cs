﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using System.Runtime.InteropServices;

var size = SmallConcurrentQueue<int>.Size;
var queue = new SmallConcurrentQueue<int>();

for (var repeat = 0; repeat < 10; repeat++)
{
    for (var i = 0; i < size; i++)
    {
        Debug.Assert(queue.TryEnqueue(i));
    }
    Debug.Assert(!queue.TryEnqueue(100));

    for (var i = 0; i < size; i++ )
    {
        Debug.Assert(queue.TryDequeue(out _));
    }
    Debug.Assert(!queue.TryDequeue(out _));
}

Console.WriteLine("OK!");

   public class SmallConcurrentQueue<T>
    {
        public const int Size = 16;
        const int SlotsMask = Size - 1;

        readonly Slot[] slots = new Slot[Size];
        PaddedHeadAndTail headAndTail;

        public SmallConcurrentQueue()
        {
            for (var i = 0; i < Size; i++)
            {
                slots[i].SequenceNumber = i;
            }
        }

        public bool TryEnqueue(T item)
        {
            var currentTail = headAndTail.Tail;
            var i = currentTail & SlotsMask;

            var sequenceNumber = Volatile.Read(ref slots[i].SequenceNumber);
            
            int diff = sequenceNumber - currentTail;
            if (diff == 0)
            {
                headAndTail.Tail += 1;

                slots[i].Item = item;

                Volatile.Write(ref slots[i].SequenceNumber, currentTail + 1);
                return true;
            }

           return false;
        }

        /// <summary>Tries to dequeue an element from the queue.</summary>
        public bool TryDequeue(out T item)
        {
            var currentHead = headAndTail.Head;
            var i = currentHead & SlotsMask;

            int sequenceNumber = Volatile.Read(ref slots[i].SequenceNumber);
            
            int diff = sequenceNumber - (currentHead + 1);
            if (diff == 0)
            {
                headAndTail.Head += 1;

                // Get the item
                item = slots[i].Item;

                // clear the item
                slots[i].Item = default;
                Volatile.Write(ref slots[i].SequenceNumber, currentHead + Size);

                return true;
            }

            item = default;
            return false;
        }

        [DebuggerDisplay("Item = {Item}, SequenceNumber = {SequenceNumber}")]
        [StructLayout(LayoutKind.Auto)]
        struct Slot
        {
            public T Item;
            public int SequenceNumber;
        }
    }

    [DebuggerDisplay("Head = {Head}, Tail = {Tail}")]
    [StructLayout(LayoutKind.Explicit, Size = 3 * AssumedMaxCacheLineSize)] // padding before/between/after fields
    struct PaddedHeadAndTail
    {
        // Common cache line sizes are 32, 64 and 128 bytes. 
        const int AssumedMaxCacheLineSize = 128;

        /// <summary>
        /// Keeps the head
        /// </summary>
        [FieldOffset(1 * AssumedMaxCacheLineSize)] public int Head;
        [FieldOffset(2 * AssumedMaxCacheLineSize)] public int Tail;
    }
