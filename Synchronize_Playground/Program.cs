﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Nito.AsyncEx;

namespace Synchronize_Playground
{
    class Program
    {
        private static object _lock = new();
        
        static async Task Main(string[] args)
        {
            var account = new Account(1000);
            var task = new Task[100];
            for (int i = 0; i < 100; i++)
            {
                task[i] = Task.Run(() => { Update(account);});
            }
            
            await Task.WhenAll(task);

            Console.WriteLine($"Account balance: {account.GetBalance()}");

            using (TimedLock.Lock(_lock, TimeSpan.FromSeconds(2)))
            {
                // Sekcja krytyczna
            }
            
            Console.ReadLine();
        }

        static void Update(Account account)
        {
            decimal[] amounts = { 0, 2, -3, 6, -2, -1, 8, -5, 11, -6 };
            foreach (var amount in amounts)
            {
                if (amount >= 0)
                {
                    account.Credit(amount);
                }
                else
                {
                    account.Debit(Math.Abs(amount));
                }
            }
        }
    }

    class Account
    {
        private static object _padlock = new();

        private static SemaphoreSlim _semaphore = new(1, 1);

        private readonly AsyncLock _lock = new();

        private decimal _balance;

        public Account(decimal balance)
        {
            _balance = balance;
        }

        public async Task<decimal> Debit(decimal amount)
        {
            if (amount < 0)
                throw new ArgumentOutOfRangeException();
            
            decimal appliedAmount = 0;

            // await _semaphore.WaitAsync();
            //
            // try
            // {
            using (await _lock.LockAsync())
            {
                if (_balance >= amount)
                {
                    await Task.Delay(1000);
                    _balance -= amount;
                    appliedAmount = amount;
                }
            }
                
            // }
            // finally
            // {
            //     _semaphore.Release();
            // }

            
            return appliedAmount;
        }

        public async Task Credit(decimal amount)
        {
            if (amount < 0)
                throw new ArgumentOutOfRangeException();

            // await _semaphore.WaitAsync();
            //
            // try
            // {
            using (await _lock.LockAsync())
            {
                await Task.Delay(1000);
                _balance += amount;
            }
            // }
            // finally
            // {
            //     _semaphore.Release();
            // }

        }

        public async Task<decimal> GetBalance()
        {
            // await _semaphore.WaitAsync();
            //
            // try
            // {
            using (await _lock.LockAsync())
            {
                await Task.Delay(1000);
                return _balance;
            }
            // }
            // finally
            // {
            //     _semaphore.Release();
            // }
        } 
    }
    
    // class Account
    // {
    //     private static object _padlock = new();
    //
    //     private static readonly ReaderWriterLockSlim _lock = new();
    //     private decimal _balance;
    //
    //     public Account(decimal balance)
    //     {
    //         _balance = balance;
    //     }
    //
    //     public decimal Debit(decimal amount)
    //     {
    //         if (amount < 0)
    //             throw new ArgumentOutOfRangeException();
    //         
    //         decimal appliedAmount = 0;
    //         
    //         _lock.EnterWriteLock();
    //
    //         try
    //         {
    //             if (_balance >= amount)
    //             {
    //                 _balance -= amount;
    //                 appliedAmount = amount;
    //             }
    //         }
    //         finally
    //         {
    //             _lock.ExitWriteLock();
    //         }
    //         // lock (_padlock)
    //         // {
    //
    //         // }
    //
    //         return appliedAmount;
    //     }
    //
    //     public void Credit(decimal amount)
    //     {
    //         if (amount < 0)
    //             throw new ArgumentOutOfRangeException();
    //
    //         _lock.EnterWriteLock();
    //
    //         try
    //         {
    //             _balance += amount;
    //         }
    //         finally
    //         {
    //             _lock.ExitWriteLock();
    //         }
    //
    //         // lock (_padlock)
    //         // {
    //             
    //         // }
    //     }
    //
    //     public decimal GetBalance()
    //     {
    //         // _lock.EnterReadLock();
    //         
    //         _lock.EnterUpgradeableReadLock();
    //         
    //         _lock.EnterWriteLock();
    //
    //         try
    //         {
    //             return _balance;
    //         }
    //         finally
    //         {
    //             _lock.ExitReadLock();
    //         }
    //         // lock (_padlock)
    //         // {
    //         // }
    //     }
    // }

    public ref struct TimedLock
    {
        private object _target;

        public TimedLock(object target)
        {
            _target = target;
        }

        public static TimedLock Lock(object o, TimeSpan timeout)
        {
            TimedLock t = new TimedLock(o);

            if (!Monitor.TryEnter(o, timeout))
            {
                throw new TimeoutException();
            }

            return t;
        }

        public void Dispose() => Monitor.Exit(_target);
    }
}