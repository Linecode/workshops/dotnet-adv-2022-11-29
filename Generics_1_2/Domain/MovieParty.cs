using System;

namespace Generics_1_2.Domain
{
    public class MovieParty
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public MoviePartyRole Role { get; set; }
    }
}