using System;
using System.Collections.Generic;
using Generics_1_2.Domain;
using Generics_1_2.Infrastructure;

namespace Generics_1_2.Commands;

public record CreateMovieCommand(Guid Id, string Title, MovieParty Director, List<MovieParty> Cast) : ICommand;

public class CreateMovieHandler : ICommandHandler<CreateMovieCommand>
{
    private readonly Repository _repository;

    public CreateMovieHandler(Repository repository)
    {
        _repository = repository;
    }

    public void Handle(CreateMovieCommand command)
    {
        var movie = new Movie
        {
            Id = command.Id,
            Title = command.Title,
            Director = command.Director,
            Cast = command.Cast
        };
        
        _repository.Add(movie);
    }
}