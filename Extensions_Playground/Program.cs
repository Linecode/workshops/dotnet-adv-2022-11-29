﻿using System;
using A;
using Newtonsoft.Json;

namespace Extensions_Playground
{
    public class Program
    {
        static void Main(string[] args)
        {
            // var person = new Person("A", "B");
            //
            // Console.WriteLine(person.FullName());

            var payload = new { Test = false }.AsJson();

            Console.WriteLine("Call http: {0}", payload);
        }
    }

    public static class IntExtensions
    {
        public static bool IsOdd(this int value)
        {
            return value % 2 == 1;
        }
    }

    public static class ObjectExtensions
    {
        public static string AsJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    } 

    public static class FluentAssertions
    {
        public static void Should(this object obj)
        {
            
        }
    }

    public static class PersonExtensions
    {
        public static string FullName(this Person person)
        {
            return Transform($"{person.Name} {person.LastName}");
        }

        private static string Transform(string transform)
        {
            return transform.ToUpperInvariant();
        }
    }
}

namespace A
{
    public class Person
    {
        public string Name { get; }
        public string LastName { get; }

        public Person(string name, string lastName)
        {
            Name = name;
            LastName = lastName;
        }
    }
}