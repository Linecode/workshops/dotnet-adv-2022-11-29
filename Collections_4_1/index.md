# Collections 4_1

Zaimplementuj własną kolekcję, która będzie przechowywać filmy.

Wymagania: 

- Kolekcja ma implementować interfejs `IEnumerable`
- Kolekcja ma posiadać w sobie prywatną klasę `MovieEnumerator` implementującą interfejs `IEnumerator`
- Kolekcja powinna wspierać pobieranie i ustawianie elementów za pomocą operator []
- Kolekcja powinna mieć zaimplementowaną metodę do dodawania kolejnych elementów `Add(Movie)`