﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Collections_4_1.Domain;

namespace Collections_4_1
{
    class Program
    {
        static void Main(string[] args)
        {

        }
    }

    public class MovieList : IEnumerable
    {
        private List<Movie> _movies = new();
        
        public IEnumerator GetEnumerator()
        {
            return new MovieEnumerator(_movies.ToArray());
        }

        public void Add(Movie movie)
            => _movies.Add(movie);

        public Movie this[int i]
        {
            get => _movies[i];
            set => _movies[i] = value;
        }
        
        private class MovieEnumerator : IEnumerator
        {
            public Movie[] Movies { get; }
            private int _position = -1;

            public MovieEnumerator(Movie[] movies)
            {
                Movies = movies;
            }
            
            public bool MoveNext()
            {
                _position++;
                return _position < Movies.Length;
            }

            public void Reset()
            {
                _position = -1;
            }

            public object Current => Movies.ElementAt(_position);
        }
    }
}