﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentCollections_1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var cq = new ConcurrentQueue<Action<int>>();

            Action<int> workload = async (id) =>
            {
                await Task.Delay(100);
                Console.WriteLine($"Action {id} handled by {Thread.CurrentThread.ManagedThreadId}");
            };
            
            var tasks = new Task[5];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Factory.StartNew(() =>
                {
                    for (var j = 0; j < 20; j++)
                    cq.Enqueue(workload);
                });
            }
            
            await Task.WhenAll(tasks);

            Action consumer = () =>
            {
                while (cq.TryDequeue(out var workItem))
                {
                    if (workItem is not null)
                        workItem(Task.CurrentId ?? 0);
                }
            };
            
            var consumerTasks = new Task[10];
            for (int i = 0; i < consumerTasks.Length; i++)
            {
                consumerTasks[i] = Task.Factory.StartNew(consumer);
            }
            
            await Task.WhenAll(consumerTasks);
            
            Console.ReadLine();
        }
    }
}