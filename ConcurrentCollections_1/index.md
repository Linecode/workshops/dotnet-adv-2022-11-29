# Zadanie 10_1

- Wykorzystując więdze zdobytą podczas tych warsztatów napisz kod, który będzie działał na zasadzie producer / consumer użyj ConcurrentQueue<Action<int>> aby przechować kolejkę zadań

Aby zakolejkować zadanie możesz wykorzysta kod:

```csharp
var cq = new ConcurrentQueue<Action<int>>();
cq.Enqueue(async(id) => { /* workload */});
```

Aby obsłużyc zadanie możesz wykorzystac kod: 

```csharp
cq.TryDequeue(out var task);
task();
```