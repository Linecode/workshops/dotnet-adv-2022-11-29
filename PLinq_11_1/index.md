# Zadanie 11_1

Napisz implementacje, która przetestuje która wersja sumowania licz pierwszych jest szybsza Linq czy Plinq czy Parallel

Aby sprawdzić czy dana liczba jest liczbą pierwszą możesz użyc następującej funkcji: 

```csharp
private static bool IsPrime(int number)
{
    if (number < 2)
    {
        return false;
    }

    for (var divisor = 2; divisor <= Math.Sqrt(number); divisor++)
    {
        if (number % divisor == 0)
        {
            return false;
        }
    }
    return true;
}
```