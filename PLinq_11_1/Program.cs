﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace PLinq_11_1
{
    public class PLinqBenchmark
    {
        private IEnumerable<int> source = Enumerable.Range(1, 10000).ToList();

        [Benchmark]
        public int Plinq()
        {
            // Implementacja Plinq
            return 0;
        }

        [Benchmark]
        public int Seq()
        {
            // Implementacja Linq
            return 0;
        }

        [Benchmark]
        public int CountSumPrimeListWithParallel()
        {
            int sum = 0;

            // Implementacja Parallel

            return sum;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run(typeof(Program).Assembly);
        }
    }
}