``` ini

BenchmarkDotNet=v0.13.1, OS=macOS Big Sur 11.5.2 (20G95) [Darwin 20.6.0]
Intel Core i7-1068NG7 CPU 2.30GHz, 1 CPU, 8 logical and 4 physical cores
.NET SDK=5.0.404
  [Host]     : .NET 5.0.13 (5.0.1321.56516), X64 RyuJIT
  DefaultJob : .NET 5.0.13 (5.0.1321.56516), X64 RyuJIT


```
|                        Method |      Mean |    Error |   StdDev |    Median |
|------------------------------ |----------:|---------:|---------:|----------:|
|                         Plinq |  91.62 μs | 1.806 μs | 1.774 μs |  92.17 μs |
|                           Seq | 272.59 μs | 4.744 μs | 8.555 μs | 268.51 μs |
| CountSumPrimeListWithParallel | 119.32 μs | 0.757 μs | 0.591 μs | 119.37 μs |
