﻿Console.WriteLine("Hello World!");

public class Repository
{
    private readonly object _padlock = new object();
    private readonly List<Person> _persons = new List<Person>()
    {
        new Person("John", 33, 1),
        new Person("Nick", 31, 2),
        new Person("Mark", 56, 3),
    };

    public void Update(Person person)
    {
        lock (_padlock) // just for us to simulate database behaviour
        {
            var freshEntity = Get(person.Id);
            if (freshEntity!.Version + 1 == person.Version) // Done in persistance UPDATE person SET .... WHERE personId = 1 and Version = 4
            {
                _persons.Remove(freshEntity);
                _persons.Add(person);
            }
        }
    }

    public Person? Get(int id) 
        => _persons.Find(x => x.Id == id);
}

public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public int Version { get; set; } = 0;

    public Person(string name, int age, int id)
    {
        Name = name;
        Age = age;
        Id = id;
    }

    public void SomeAction()
    {
        Age += 1;
        Version += 1;
    }
}















