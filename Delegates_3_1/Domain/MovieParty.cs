using System;

namespace Delegates_3_1.Domain
{
    public class MovieParty
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public MoviePartyRole Role { get; set; }
    }
}