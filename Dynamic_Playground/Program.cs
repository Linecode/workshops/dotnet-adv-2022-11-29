﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Reflection;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Dynamic_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // person/12
            // {
            //   "name": "John",
            //   "age": 12
            // }
            // person/13?fields=name
            // { "name": "John" }

            var person = new Person
            {
                Name = "John",
                Age = 12,
                LastName = "Doe"
            };

            var eo = person.ShapeData("");
            
            ((IDictionary<string, object>)eo)
                .Add("Links", new { prev = "https://costam", next ="http://costam" });
            
            Console.WriteLine(JsonConvert.SerializeObject(eo));

            Console.WriteLine("Hello World!");

            var eventsLists = new List<IDomainEvent>
            {
                new Aggregate.Events.CreateEvent(),
                new Aggregate.Events.UpdateEvent(),
                new Aggregate.Events.UpdateEvent(),
                new Aggregate.Events.UpdateEvent(),
                new Aggregate.Events.UpdateEvent(),
                new Aggregate.Events.UpdateEvent()
            };

            var aggregate = new Aggregate();

            foreach (var @event in eventsLists)
            {
                aggregate.Apply(@event);
            }
        }
    }

    public static class ObjectExtensions
    {
        public static ExpandoObject ShapeData<TSource>(this TSource source, string fields)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            var dataShapedObject = new ExpandoObject();

            if (string.IsNullOrWhiteSpace(fields))
            {
                var propertyInfos =
                    typeof(TSource).GetProperties(BindingFlags.Instance | BindingFlags.Public |
                                                  BindingFlags.IgnoreCase);

                foreach (var propertyInfo in propertyInfos)
                {
                    var propertyValue = propertyInfo.GetValue(source);
                    ((IDictionary<string, object>)dataShapedObject)
                        .Add(propertyInfo.Name, propertyValue);
                }
            }
            else
            {
                var fieldsAfterSplit = fields.Split(",");

                foreach (var field in fieldsAfterSplit)
                {
                    var propertyName = field.Trim();
                    var propertyInfo = typeof(TSource)
                        .GetProperty(propertyName,
                            BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
                    
                    if (propertyInfo is null)
                        throw new Exception($"Property {propertyName} not found");
                    
                    var propertyValue = propertyInfo.GetValue(source);
                    ((IDictionary<string, object>)dataShapedObject)
                        .Add(propertyName, propertyValue);
                }
            }

            return dataShapedObject;
        }
    }

    public class Person
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
    
    public interface IDomainEvent {}

    public class Aggregate
    {
        public void Apply(IDomainEvent @event)
        {
            When((dynamic)@event);
        }

        private void When(Events.CreateEvent @event)
        {
            Console.WriteLine("Create Event was applied");
        }

        private void When(Events.UpdateEvent @event)
        {
            Console.WriteLine("Update Event was applied");
        }

        public static class Events
        {
            public record CreateEvent() : IDomainEvent;
            public record UpdateEvent() : IDomainEvent;
        }
    }
}
