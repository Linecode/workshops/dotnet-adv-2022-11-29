﻿using System;

namespace Delegates_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            // SomeEntity.OnCreation callbacks = delegate(SomeEntity entity) { Console.WriteLine("Here"); };
            // callbacks += delegate(SomeEntity entity) { Console.WriteLine("Here2"); };
            //
            // new SomeEntity(callbacks);
            
            Func<int, int> makeDouble = x => x * 2;
            Func<int, int> makeTriple = x => x * 3;
            Func<int, string> toString = x => x.ToString();
            Func<int, string> makeTimesSixString = toString.Compose(makeDouble).Compose(makeTriple);
            
            Console.WriteLine(makeTimesSixString(3));

            var result = new SomeService().Test();

            result.Match(
                OnSuccess: _ =>
                {
                    Console.WriteLine(_);
                    return true;
                },
                OnError: _ => false
            );
        }
    }

    public class SomeService
    {
        public Result<int> Test()
        {
            return Result<int>.Success(10);
        }
    }

    public class Person
    {
        public int Age { get; set; }
        
        public bool IsOver18 => Age > 18;
    }

    public static class FuncExtensions
    {
        public static Func<T, TReturn2> Compose<T, TReturn1, TReturn2>(this Func<TReturn1, TReturn2> func1,
            Func<T, TReturn1> func2)
        {
            return x => func1(func2(x));
        }
    }

    public class SomeEntity
    {
        public delegate void OnCreation(SomeEntity entity);
        
        public SomeEntity(OnCreation onCreation)
        {
            onCreation(this);
        }
    }
    
    public class Result
    {
        public bool IsSuccessful { get; }
        public Exception Exception { get; }

        private protected Result()
        {
            IsSuccessful = true;
        }

        private protected Result(Exception error)
        {
            IsSuccessful = false;
            Exception = error;
        }

        public static Result Success() => new ();
        public static Result Error(Exception error) => new (error);
    }
    
    public class Result<T> : Result
    {
        private T Data { get; }
        
        private Result(T data)
        {
            Data = data;
        }

        private Result(Exception e) : base(e)
        { }

        public TResult Match<TResult>(Func<T, TResult> OnSuccess, Func<Exception, TResult> OnError)
        {
            if (IsSuccessful) return OnSuccess(Data);
            return OnError(Exception);
        }

        public static Result<T> Success(T data) => new (data);
        public new static Result<T> Error(Exception error) => new (error);
    }
}