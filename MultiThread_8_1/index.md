# Zadanie 8_1

Za pomocą api Thread (klasa `Thread`), stwórz dwa wątki background:
 - Pierwszy z nich niech odlicza co sekundę w dół
 - Drugi z nich niech odlicza co sekundę w górę

Wymagania:
- W momencie kiedy pierwszy wątek osiągnie liczbę -10 lub został wywołany cancel na cancelletion Token zakończ wątek 
- W momencie kiedy drugi wątek osiągnie liczbę 15 lub został wywołany cancel na cancelletion Token zakończ wątek