﻿using System;
using System.Threading;

namespace MultiThread_8_1
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                Console.WriteLine("Not handled error inside thread");
            };

            var cts1 = new CancellationTokenSource();
            cts1.CancelAfter(500);
            var cts2 = new CancellationTokenSource();
            cts2.CancelAfter(500);

            var thread1 = new Thread((ctsobj) =>
            {
                var i = 0;
                var token = (CancellationToken)ctsobj;
                
                while (!token.IsCancellationRequested && i < 15)
                {
                    i++;
                    Thread.Sleep(100);

                    DumpThreadInfo($"T2 - {i}");
                }

                Console.WriteLine("Cancellation request!");
            });
            var thread2 = new Thread((ctsobj) =>
            {
                var i = 0;
                var token = (CancellationToken)ctsobj;

                while (!token.IsCancellationRequested && i > -10)
                {
                    i--;
                    Thread.Sleep(100);
                    
                    DumpThreadInfo($"T1 - {i}");
                    Console.WriteLine("Cancellation request!");
                }
            });
            
            thread1.Start(cts1.Token);
            thread2.Start(cts2.Token);

            Console.ReadLine();
        }

        static void DumpThreadInfo(string label)
        {
            Console.WriteLine($"Thread: {label}: {Thread.CurrentThread.ManagedThreadId} " +
                              $"isThreadPoolThread: {Thread.CurrentThread.IsThreadPoolThread} " +
                              $"isBackground: {Thread.CurrentThread.IsBackground}");
        }
    }
}