﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Multithread_Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                Console.WriteLine("Oh no! :(");
            };

            // var t1 = new Thread((object label) =>
            // {
            //     var i = 0;
            //     while (i < 15)
            //     {
            //         i++; 
            //         DumpThread($"{(string) label} - {i}");
            //         
            //         Thread.Sleep(1000);
            //     }
            //     
            //     Console.WriteLine("after while t1");
            // })
            // {
            //     Name = "T1",
            //     IsBackground = false
            // };
            //
            // var t2 = new Thread((object label) =>
            // {
            //     int i = 0;
            //     while (i > -15)
            //     {
            //         i--;
            //         DumpThread($"{(string) label} - {i}");
            //         
            //         Thread.Sleep(1000);
            //     }
            //
            //     Console.WriteLine("after while t2");
            // })
            // {
            //     IsBackground = false,
            //     Name = "T2",
            //     Priority = ThreadPriority.AboveNormal
            // };
            //
            // t1.Start("T1");
            // t2.Start("T2");
            //
            // t1.Join();
            // t2.Join();

            // var wait = new SpinWait();
            //
            // var t1 = new Thread(() =>
            // {
            //     var i = 0;
            //
            //     while (true)
            //     {
            //         DumpThread(i.ToString());
            //         i++;
            //
            //         if (wait.NextSpinWillYield)
            //         {
            //             Console.WriteLine("Will yield, abort!");
            //             break;
            //         }
            //         else
            //         {
            //             wait.SpinOnce();
            //         }
            //     }
            // }) { IsBackground = true };
            //
            // t1.Start();
            // t1.Join();

            // using var cts = new CancellationTokenSource();
            // cts.CancelAfter(3000);
            //
            // var reg = cts.Token.Register(() =>
            // {
            //     Console.WriteLine("Cancellation Requested! From cts Token event");
            // });
            //
            // var t1 = new Thread((object ctsObj) =>
            // {
            //     var token = (CancellationToken)ctsObj;
            //
            //     while (!token.IsCancellationRequested)
            //     {
            //         DumpThread("T1");
            //         
            //         Thread.Sleep(1000);
            //     }
            //     
            //     Console.WriteLine("Cancellation Requested!");
            // });
            //
            // t1.Start(cts.Token);
            // t1.Join();

            // var t1 = new Thread(() =>
            // {
            //     while (true)
            //     {
            //         ThreadSafeCounter.Value++;
            //         Console.WriteLine($"T1 set counter: {ThreadSafeCounter.Value}");
            //
            //         Thread.Sleep(200);
            //         Console.WriteLine($"T1 set counter: {ThreadSafeCounter.Value}");
            //     }
            // });
            //
            // var t2 = new Thread(() =>
            // {
            //     while (true)
            //     {
            //         ThreadSafeCounter.Value++;
            //         Console.WriteLine($"T2 set counter: {ThreadSafeCounter.Value}");
            //
            //         Thread.Sleep(200);
            //         Console.WriteLine($"T2 set counter: {ThreadSafeCounter.Value}");
            //     }
            // });
            //
            // t1.Start();
            // t2.Start();

            // ThreadLocal<string> ThreadName = new ThreadLocal<string>(() =>
            // {
            //     return "Thread " + Thread.CurrentThread.ManagedThreadId;
            // });
            //
            // Action action = () =>
            // {
            //     var repeat = ThreadName.IsValueCreated;
            //
            //     Console.WriteLine("ThreadName = {0} {1} {2}", ThreadName.Value, repeat ? "(repeat)" : "", Thread.CurrentThread.IsThreadPoolThread);
            // };
            //
            // Parallel.Invoke(action, action, action, action, action, action, action, action, action, action, action, action);

            // Console.ReadLine();
            // cts.Cancel();
            
            ThreadPool.GetMaxThreads(out var maxWorkerThreads, out var maxIocpThreads);
            ThreadPool.GetMinThreads(out var minWorkerThreads, out var minIocpThreads);
            
            Console.WriteLine($"WorkerThreads = ({minWorkerThreads},{maxWorkerThreads})");
            Console.WriteLine($"IocpThreads = ({minIocpThreads},{maxIocpThreads})");

            var statsThread = new Thread(PrintThreadStatus);
            var spawnThread = new Thread(SpawnWork); 
            
            statsThread.Start();
            spawnThread.Start();
            
            // reg.Dispose();
            Console.ReadLine();
        }

        private static void SpawnWork(object obj)
        {
            while (true)
            {
                var cts = new CancellationTokenSource();
                Thread.Sleep(100);

                ThreadPool.QueueUserWorkItem((ctsobj) =>
                {
                    var token = (CancellationToken)ctsobj;
                    Thread.Sleep(60000);
                }, cts.Token);
            }
        }

        private static void PrintThreadStatus(object obj)
        {
            while (true)
            {
                var queuedItemsCount = ThreadPool.PendingWorkItemCount;
                var completedItemsCount = ThreadPool.CompletedWorkItemCount;
                var threadsCount = ThreadPool.ThreadCount;
                ThreadPool.GetAvailableThreads(out var workerThreads, out var iocpThreads);

                Console.WriteLine(
                    $"Current Threads: {threadsCount} Queued Items: {queuedItemsCount}, Completed Items: {completedItemsCount}, Available (W: {workerThreads} I: {iocpThreads})");
                Thread.Sleep(200);
            }
        }

        static void DumpThread(string label)
        {
            Console.WriteLine($"[{DateTime.Now:hh:mm:ss.fff}] {label}: " +
                              $"ID: {Thread.CurrentThread.ManagedThreadId} " +
                              $"IsOnThreadPool: {Thread.CurrentThread.IsThreadPoolThread}");
        }

        public static class ThreadSafeCounter
        {
            [ThreadStatic]
            public static int Value;
        }
    }
}
