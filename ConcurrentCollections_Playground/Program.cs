﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentCollections_Playground
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // var c = new BlockingCollection<int>(boundedCapacity: 4);
            // // var c = new BlockingCollection<int>(new ConcurrentStack<int>());
            // var d = new BlockingCollection<int>(boundedCapacity: 4);
            // var e = new BlockingCollection<int>(boundedCapacity: 4);
            // c.Add(1);
            // c.Add(2);
            //
            // c.TryTake(out var a);
            // c.TryTake(out var b);
            //
            // // c.CompleteAdding();
            //
            // // c.Add(1);
            //
            // Console.WriteLine($"IsCompleted: {c.IsCompleted} IsAddingCompleted: {c.IsAddingCompleted}");
            //
            // BlockingCollection<int>.AddToAny(new[] { c, d, e }, 10);
            //
            // BlockingCollection<int>.TakeFromAny(new [] { c, d, e }, out var g);
            //
            // Console.WriteLine($"{g}");

            //     var item = 10000;
            //     var cs = new ConcurrentStack<int>();
            //
            //     Action<bool> popOrPush = (bool flag) =>
            //     {
            //         if (flag)
            //         {
            //             for (int i = 0; i < item; i++)
            //             {
            //                 cs.Push(i);
            //                 Console.WriteLine(
            //                     $"Thread: {Thread.CurrentThread.ManagedThreadId} pushed {i} to concurrent stack");
            //             }
            //         }
            //         else
            //         {
            //             for (int i = 0; i < item; i++)
            //             {
            //                 cs.TryPop(out var item);
            //                 Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId} popped {i}");
            //             }
            //         }
            //     };
            //
            //     var tasks = new Task[20];
            //     for (int i = 0; i < tasks.Length; i++)
            //     {
            //         var rnd = new Random().Next(1,3);
            //         tasks[i] = Task.Factory.StartNew(() =>
            //         {
            //             popOrPush(rnd % 2 == 0);
            //         });
            //     }

            //     var cq = new ConcurrentQueue<int>();
            //     
            //     for (int i = 0; i < 10000; i++)
            //         cq.Enqueue(i);
            //
            //     int outerSum = 0;
            //
            //     var action = () =>
            //     {
            //         int localSum = 0;
            //         while (cq.TryDequeue(out var item))
            //             localSum += item;
            //
            //         Interlocked.Add(ref outerSum, localSum);
            //     };
            //
            //     var tasks = new Task[4];
            //     for (int i = 0; i < tasks.Length; i++)
            //         tasks[i] = Task.Factory.StartNew(action);
            //
            //     await Task.WhenAll(tasks);
            //     
            //     Console.WriteLine($"Outer sum: {outerSum}");

            // var cb = new ConcurrentBag<int>();
            // var bagAndTasks = new List<Task>();
            //
            // for (int i = 0; i < 500; i++)
            // {
            //     var numberToAdd = i;
            //     bagAndTasks.Add(Task.Run(() => cb.Add(numberToAdd)));
            // }
            //
            // await Task.WhenAll(bagAndTasks);
            //
            // var bagConsumers = new List<Task>();
            // int itemsInBag = 0;
            //
            // while (!cb.IsEmpty)
            // {
            //     bagConsumers.Add(Task.Run(() =>
            //     {
            //         int item;
            //         if (cb.TryTake(out item))
            //         {
            //             Console.WriteLine(item);
            //             Interlocked.Increment(ref itemsInBag);
            //         }
            //     }));
            // }
            //
            // await Task.WhenAll(bagConsumers);
            //
            // Console.WriteLine($"Items in bag: {itemsInBag}");

            
            // int numItems = 64;
            // int initialCapacity = 1024;
            //
            // var numProcs = Environment.ProcessorCount;
            // var concurrencyLevel = numProcs * 2;
            //
            // var cd = new ConcurrentDictionary<int, int>(concurrencyLevel, initialCapacity);
            //
            // for (int i = 0; i < numItems; i++)
            // {
            //     cd[i] = i * i;
            // }
            //
            // Console.WriteLine("The square of 23 is {0} / {1}", cd[23], 23 * 23);

            var cache = new ConcurrentDictionary<string, object>();

            var value = new { };
            cache.AddOrUpdate("smth", value, (s, o) => value);
        }
    }
}


















